package com.example.photocontestproject.services.contracts;

import com.example.photocontestproject.models.Photo;
import com.example.photocontestproject.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface PhotoService {

    List<Photo> getAll();

    Photo getById(int id);

    Photo getByPath(String path);

    List<Photo> getCoverPhotos();

    void create(Photo photo);

    void delete(int id, User loggedUser);

    void writeFile(MultipartFile file,boolean isCover) throws IOException;

    void update(Photo photo, User loggedUser);
}
