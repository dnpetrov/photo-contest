package com.example.photocontestproject.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "entries")
public class Entry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "entry_id")
    private int id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "contest_id")
    private Contest contest;

    @Column(name = "title")
    @NotNull(message = "Title field cannot be empty")
    private String title;

    @Column(name = "story")
    @NotNull(message = "Story field cannot be empty")
    private String story;

    @Column(name = "is_invited")
    private boolean isInvited;

    @OneToOne
    @JoinColumn(name = "photo_id")
    private Photo photo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User participant;

    @JsonIgnore
    @OneToMany(mappedBy = "entry", fetch = FetchType.EAGER)
    @MapKeyColumn(name = "review_id")
    private Set<Review> reviews;

    @JsonIgnore
    public boolean isRatedBy(User user) {
        return reviews.stream().anyMatch(review -> review.getJuror().equals(user));
    }

    @JsonIgnore
    public Double getRatingFor(User user) {
        return reviews.stream()
                .filter(review -> review.getJuror().equals(user))
                .map(Review::getScore)
                .map(Integer::doubleValue).findAny()
                .orElseThrow(() -> new IllegalStateException("User has not rated this entry"));

    }

    public Entry() {
    }

    public Entry(int id, Contest contest, String title, String story, boolean isInvited, Photo photo, User participant) {
        this.id = id;
        this.contest = contest;
        this.title = title;
        this.story = story;
        this.isInvited = isInvited;
        this.photo = photo;
        this.participant = participant;
        reviews = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public boolean isInvited() {
        return isInvited;
    }

    public void setInvited(boolean invited) {
        isInvited = invited;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public User getParticipant() {
        return participant;
    }

    public void setParticipant(User participant) {
        this.participant = participant;
    }

    public Set<Review> getReview() {
        return reviews;
    }

    public void setReview(Set<Review> reviews) {
        this.reviews = reviews;
    }

    public void createReview(Review review) {
        reviews.add(review);
    }

    public void deleteReview(Review review) {
        reviews.remove(review);
    }

    public boolean entryReviewed(User user) {
        return reviews.stream().anyMatch(rev -> rev.getJuror().equals(user));
    }

}
