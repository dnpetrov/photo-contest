package com.example.photocontestproject.controllers.mvc;

import com.example.photocontestproject.controllers.AuthenticationHelper;
import com.example.photocontestproject.exceptions.AuthenticationFailureException;
import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.dtos.FilterUserDto;
import com.example.photocontestproject.models.dtos.UpdateUserDto;
import com.example.photocontestproject.models.dtos.UserDto;
import com.example.photocontestproject.models.enums.Ranking;
import com.example.photocontestproject.models.enums.UserSortOptions;
import com.example.photocontestproject.services.contracts.ContestService;
import com.example.photocontestproject.services.contracts.UserService;
import com.example.photocontestproject.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.photocontestproject.utils.constants.GlobalConstants.ORGANIZER_ROLE;

@Controller
@RequestMapping("/users")
public class UserMVCController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;

    @Autowired
    public UserMVCController(UserService userService, UserMapper userMapper,
                             AuthenticationHelper authenticationHelper, ContestService contestService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganizer")
    public boolean populateIsOrganizer(HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            return loggedUser.getRole().getRole().equals(ORGANIZER_ROLE);
        } catch (AuthenticationFailureException | EntityNotFoundException | NullPointerException e) {
            return false;
        }
    }

    @ModelAttribute("sortUserOptions")
    public UserSortOptions[] userSortOptions() {
        return UserSortOptions.values();
    }

    @ModelAttribute("ranks")
    public Ranking[] fetchAllRanks() {
        return Ranking.values();
    }

    @PostMapping()
    public String filterUsers(@ModelAttribute("filterUserDto") FilterUserDto filterUserDto, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.verifyOrganizerAuthorization(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        var filtered = userService.filter(
                Optional.ofNullable(filterUserDto.getName().isBlank() ? null : filterUserDto.getName()),
                Optional.ofNullable(filterUserDto.getRanking().equals("") ?
                        null : Ranking.valueOf(filterUserDto.getRanking().toUpperCase())),
                Optional.ofNullable(filterUserDto.getSort() == null ?
                        null : UserSortOptions.valueOfPreview(filterUserDto.getSort()))
        );
        model.addAttribute("users", filtered);
        return "users";
    }

    @GetMapping()
    public String showAllUsers(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.verifyOrganizerAuthorization(session);
            model.addAttribute("loggedUser", user);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        List<User> users = new ArrayList<>();
        users = userService.getAll(user);
        String search = "search";
        model.addAttribute("users", users);
        model.addAttribute("search", Optional.of(search));
        model.addAttribute("filterUserDto", new FilterUserDto());
        return "users";
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.verifyUserProfile(session, id);
            model.addAttribute("loggedUser", user);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            List<Contest> phaseOnes;
            List<Contest> openUserContests;
            List<Contest> finishedUserContests;
            try {
                phaseOnes = contestService.getAllPhaseOne(0 ,user);
            } catch (EntityNotFoundException e) {
                phaseOnes = new ArrayList<>();
            }
            try {
                openUserContests = contestService.getOpenByUser(user);
            } catch (EntityNotFoundException e) {
                openUserContests = new ArrayList<>();
            }
            try {
                finishedUserContests = contestService.getFinishedByUser(user);
            } catch (EntityNotFoundException e) {
                finishedUserContests = new ArrayList<>();
            }
            model.addAttribute("phaseOnes", phaseOnes);
            model.addAttribute("openUserContests", openUserContests);
            model.addAttribute("finishedUserContests", finishedUserContests);

            user = userService.getById(id, user);
            model.addAttribute("user", user);
            boolean updateUserIsOrganizer = user.getRole().getRole().equals(ORGANIZER_ROLE);
            model.addAttribute("updateUserIsOrganizer", updateUserIsOrganizer);
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/register")
    public String showNewUserPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "registration";
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.verifyUpdateUser(session, id);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("loggedUser", user);
        try {
            user = userService.getById(id, user);
            UpdateUserDto userDto = userMapper.fromUser(user);
            model.addAttribute("id", id);
            model.addAttribute("user", userDto);
            return "user-profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id, @Valid @ModelAttribute("user") UpdateUserDto userDto,
                             BindingResult errors, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.verifyUpdateUser(session, id);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "user-profile";
        }

        try {
            User user = userMapper.userFromDto(userDto, id);
            userService.update(user, loggedUser);

            return "redirect:/users/" + id;
        } catch (DuplicateEntityException e) {
            errors.rejectValue("username", "duplicate_user", e.getMessage());
            return "user-profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/promote")
    public String promoteUser(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getById(id);
            userService.promote(loggedUser, user);

            return "redirect:/users/" + id;

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            userService.delete(id, loggedUser);

            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }
}
