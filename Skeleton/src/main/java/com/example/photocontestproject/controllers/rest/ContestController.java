package com.example.photocontestproject.controllers.rest;


import com.example.photocontestproject.controllers.AuthenticationHelper;
import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.dtos.ContestDto;
import com.example.photocontestproject.models.enums.Phase;
import com.example.photocontestproject.services.contracts.ContestService;
import com.example.photocontestproject.utils.mappers.ContestMapper;
import com.example.photocontestproject.utils.mappers.PhotoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.example.photocontestproject.utils.constants.GlobalConstants.PLEASE_SELECT_A_FILE_TO_UPLOAD;

@RestController
@RequestMapping("/api/contests")
public class ContestController {

    private final ContestService contestService;
    private final ContestMapper contestMapper;
    private final AuthenticationHelper authenticationHelper;
    private final PhotoMapper photoMapper;

    @Autowired
    public ContestController(ContestService contestService, ContestMapper contestMapper, AuthenticationHelper authenticationHelper, PhotoMapper photoMapper) {
        this.contestService = contestService;
        this.contestMapper = contestMapper;
        this.authenticationHelper = authenticationHelper;
        this.photoMapper = photoMapper;
    }

    @GetMapping()
    public List<Contest> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return contestService.getAll();
    }

    @GetMapping("/{id}")
    public Contest getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        authenticationHelper.tryGetUser(headers);
        try {
            return contestService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("filter")
    public List<Contest> filter(@RequestHeader HttpHeaders headers, @RequestParam(required = false) Optional<String> title,
                                @RequestParam(required = false) Optional<String> openAccess,
                                @RequestParam(required = false) Optional<Phase> phase,
                                @RequestParam(required = false) Optional<Integer> categoryId) {
        authenticationHelper.tryGetUser(headers);
        try {
            return contestService.filter(title, openAccess, phase, categoryId);
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping
    public Contest create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ContestDto contestDto) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        Contest contest = contestMapper.dtoToObject(contestDto);
        try {
            contestService.create(contest, loggedUser);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return contestService.getByTitle(contest.getTitle());
    }

    @PostMapping("/{id}/cover")
    public Contest submitCoverPhoto(@PathVariable int id,
                                    @RequestParam("file") MultipartFile file,
                                    @RequestParam(required = false) Optional<String> url,
                                    @RequestParam(required = false) Optional<Integer> photoId,
                                    @RequestHeader HttpHeaders headers) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        Contest contest;
        try {
            contest = contestService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        if (!file.isEmpty()) {
            contest.setCoverPhoto(photoMapper.dtoToObjectCover(file));
        } else if (url.isPresent()) {
            contest.setCoverPhoto(photoMapper.dtoToObjectCover(url));
        } else if (photoId.isPresent()) {
            contest.setCoverPhoto(photoMapper.dtoToObjectCoverPhotoId(photoId));
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, PLEASE_SELECT_A_FILE_TO_UPLOAD);
        }
        contestService.update(contest,loggedUser);
        return contestService.getById(id);
    }


    @PostMapping("/{id}")
    public Contest update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody ContestDto contestDto) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        try {
            Contest contest = contestMapper.dtoToObject(contestDto,id);
            contestService.update(contest,loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        return contestService.getById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        try {
            contestService.delete(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
