package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Review;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.repositories.contracts.EntryRepository;
import com.example.photocontestproject.repositories.contracts.ReviewRepository;
import com.example.photocontestproject.repositories.contracts.UserRepository;
import com.example.photocontestproject.services.contracts.ReviewService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static com.example.photocontestproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class EntryServiceTest {

    @Mock
    EntryRepository mockRepository;

    @Mock
    ReviewRepository reviewMockRepository;

    @Mock
    UserRepository userMockRepository;

    @Mock
    ReviewService reviewMockService;

    @InjectMocks
    EntryServiceImpl entryService;

    @Test
    public void getAll_Should_ReturnAllEntries_When_EntryExist() {
        //Arrange
        Mockito.when(mockRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        entryService.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_Should_Throw_When_EntryDoesNotExist() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenThrow(EntityNotFoundException.class);

        //Act
        entryService.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnEntry_When_EntryExist() {
        //Arrange
        Entry mockEntry = createMockEntry();
        Mockito.when(mockRepository.getById(mockEntry.getId())).thenReturn(null);

        //Act
        entryService.getById(mockEntry.getId());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(mockEntry.getId());
    }

    @Test
    public void getById_Should_Throw_When_EntryDoesNotExist() {
        //Arrange
        Entry mockEntry = createMockEntry();
        Mockito.when(mockRepository.getById(mockEntry.getId()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> entryService.getById(mockEntry.getId()));
    }

    @Test
    public void getByTitle_Should_ReturnEntry_When_EntryExist() {
        //Arrange
        Entry mockEntry = createMockEntry();
        Mockito.when(mockRepository.getByField("title", mockEntry.getTitle())).thenReturn(null);

        //Act
        entryService.getByTitle(mockEntry.getTitle());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getByField("title", mockEntry.getTitle());
    }

    @Test
    public void getByTitle_Should_Throw_When_EntryDoesNotExist() {
        //Arrange
        Entry mockEntry = createMockEntry();
        Mockito.when(mockRepository.getByField("title", mockEntry.getTitle()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> entryService.getByTitle(mockEntry.getTitle()));
    }

    @Test
    public void getAllForOpenContests_Should_ReturnEntry_When_EntryExist() {
        //Arrange
        Mockito.when(mockRepository.getAllForOpenContests()).thenReturn(null);

        //Act
        entryService.getAllForOpenContests();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllForOpenContests();
    }

    @Test
    public void getAllForOpenContests_Should_Throw_When_EntryDoesNotExist() {
        //Arrange
        Mockito.when(mockRepository.getAllForOpenContests())
                .thenThrow(EntityNotFoundException.class);

        //Act
        entryService.getAllForOpenContests();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAllForOpenContests();
    }

    @Test
    public void getAllByUsers_Should_ReturnEntry_When_EntryExist() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getAllByUser(mockUser)).thenReturn(null);

        //Act
        entryService.getAllByUser(mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllByUser(mockUser);
    }

    @Test
    public void getAllByUsers_Should_Throw_When_EntryDoesNotExist() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getAllByUser(mockUser))
                .thenThrow(EntityNotFoundException.class);

        //Act
        entryService.getAllByUser(mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAllByUser(mockUser);
    }

    @Test
    public void getAllByContestId_Should_ReturnEntry_When_EntryExist() {
        //Arrange
        Entry mockEntry = createMockEntry();
        Mockito.when(mockRepository.getAllByContestId(mockEntry.getId())).thenReturn(null);

        //Act
        entryService.getAllByContestId(mockEntry.getId());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllByContestId(mockEntry.getId());
    }

    @Test
    public void getAllByContestId_Should_Throw_When_EntryDoesNotExist() {
        //Arrange
        Entry mockEntry = createMockEntry();
        Mockito.when(mockRepository.getAllByContestId(mockEntry.getId()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        entryService.getAllByContestId(mockEntry.getId());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAllByContestId(mockEntry.getId());
    }

    @Test
    public void getByUserAndContestId_Should_ReturnEntry_When_EntryExist() {
        //Arrange
        Entry mockEntry = createMockEntry();
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getByUserAndContestId(mockUser, mockEntry.getId())).thenReturn(null);

        //Act
        entryService.getByUserAndContestId(mockUser, mockEntry.getId());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getByUserAndContestId(mockUser, mockEntry.getId());
    }

    @Test
    public void getByUserAndContestId_Should_Throw_When_EntryDoesNotExist() {
        //Arrange
        Entry mockEntry = createMockEntry();
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getByUserAndContestId(mockUser, mockEntry.getId()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> entryService.getByUserAndContestId(mockUser, mockEntry.getId()));
    }

    @Test
    public void getCountAll_Should_ReturnContest_When_EntryExist() {
        //Act
        entryService.getCountAllEntries();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getCountAllEntries();
    }

    @Test
    public void getCountByContestId_Should_ReturnContest_When_EntryExist() {
        //Act
        Contest mockContest = createMockOpenContest();
        entryService.getCountByContestId(mockContest.getId());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getCountByContestId(mockContest.getId());
    }

    @Test
    public void getCountByContestId_Should_Throw_When_EntryDoesNotExist() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        Mockito.when(mockRepository.getCountByContestId(mockContest.getId()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        entryService.getCountByContestId(mockContest.getId());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getCountByContestId(mockContest.getId());
    }

    @Test
    public void createEntry_Should_Not_CallRepository_WhenUserAlreadyParticipate() {
        //Arrange
        Entry mockEntry = createMockEntry();
        User mockUser = createMockUser();

        //Act
        entryService.create(mockEntry, mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(0)).create(mockEntry);
    }

    @Test
    public void createEntry_Should_CallRepository() {
        //Arrange
        Entry mockEntry = createMockEntry();
        User mockUser = createMockUser();
        Mockito.when(entryService.getByUserAndContestId(mockUser, mockEntry.getId())).thenThrow(new EntityNotFoundException());

        //Act
        entryService.create(mockEntry, mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).create(mockEntry);
    }

    @Test
    public void updateEntry_Should_Throw_When_UserIsNotOrganizer() {
        //Arrange
        Entry mockEntry = createMockEntry();
        User mockUser = createMockUser();

        //Act, Assert
        Mockito.verify(mockRepository, Mockito.times(0)).update(mockEntry);
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> entryService.update(mockEntry, mockUser));
    }

    @Test
    public void updateEntry_Should_CallRepository() {
        //Arrange
        Entry mockEntry = createMockEntry();
        User mockOrganizer = createMockOrganizer();

        //Act
        entryService.update(mockEntry, mockOrganizer);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).update(mockEntry);
    }

    @Test
    public void deleteEntry_Should_Throw_When_UserIsNotOrganizer() {
        //Arrange
        List<User> users = new ArrayList<>();
        Entry mockEntry = createMockEntry();
        User mockUser = createMockUserTest();
        Mockito.when(mockRepository.getById(mockEntry.getId()))
                .thenReturn(mockEntry);
        Mockito.when(userMockRepository.getContestJury(mockEntry.getContest()))
                .thenReturn(users);

        //Act, Assert
        Mockito.verify(mockRepository, Mockito.times(0)).delete(mockEntry.getId());
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> entryService.delete(mockEntry.getId(), mockUser));
    }

    @Test
    public void delete_should_callEntryRepository_whenThereAreReviews() {
        //Arrange
        List<User> users = new ArrayList<>();
        User mockOrganizer = createMockOrganizer();
        users.add(mockOrganizer);
        Entry mockEntry = createMockEntry();
        Review mockReview = createMockReview();
        List<Review> reviews = new ArrayList<>();
        reviews.add(mockReview);
        Mockito.when(userMockRepository.getContestJury(mockEntry.getContest())).thenReturn(users);
        Mockito.when(mockRepository.getById(mockEntry.getId()))
                .thenReturn(mockEntry);
        Mockito.when(mockRepository.getEntryReviews(mockEntry))
                .thenReturn(reviews);

        // Act
        entryService.delete(mockEntry.getId(), mockOrganizer);

        //Assert
        Mockito.verify(reviewMockRepository, Mockito.times(1)).delete(mockReview.getId());
    }

    @Test
    public void deleteEntry_Should_CallRepository() {
        //Arrange
        List<User> users = new ArrayList<>();
        User mockOrganizer = createMockOrganizer();
        users.add(mockOrganizer);
        Entry mockEntry = createMockEntry();
        List<Review> reviews = new ArrayList<>();
        Mockito.when(userMockRepository.getContestJury(mockEntry.getContest())).thenReturn(users);
        Mockito.when(mockRepository.getById(mockEntry.getId()))
                .thenReturn(mockEntry);
        Mockito.when(mockRepository.getEntryReviews(mockEntry))
                .thenReturn(reviews);
        //Act
        entryService.delete(mockEntry.getId(), mockOrganizer);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).delete(mockEntry.getId());
    }

    @Test
    public void search_Should_Throw_WhenUserNotOrganizer() {
        //Arrange
        User mockUser = createMockUser();
        Optional<String> search = Optional.of("mockString");

        // Act
        //Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> entryService.search(search, mockUser));
    }

    @Test
    public void search_Should_Call_Repository_WhenSearchNotEmpty() {
        // Arrange
        User mockUser = createMockOrganizer();
        Optional<String> search = Optional.of("mockSearch");
        Mockito.when(mockRepository.search(search))
                .thenReturn(new ArrayList<>());

        // Act
        entryService.search(search, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).search(search);
    }

    @Test
    public void search_Should_Call_Repository_WhenSearchEmpty() {
        // Arrange
        User mockUser = createMockOrganizer();
        Optional<String> search = Optional.empty();
        Mockito.when(mockRepository.search(search))
                .thenReturn(new ArrayList<>());

        // Act
        entryService.search(search, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).search(search);
    }

    @Test
    public void search_Should_Call_Repository_WhenSearchNull() {
        // Arrange
        User mockUser = createMockOrganizer();
        Optional<String> search = Optional.empty();
        Mockito.when(mockRepository.search(search))
                .thenReturn(new ArrayList<>());

        // Act
        entryService.search(search, mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).search(search);
    }

    @Test
    public void getFinalScore_Should_Call_Repository() {
        // Arrange
        HashMap<Entry, Double> scores = new HashMap<>();
        List<Entry> entries = new ArrayList<>();
        Entry mockEntry = createMockEntry();
        Review mockReview = createMockReview();
        HashSet<Review> reviews = new HashSet<>();
        reviews.add(mockReview);
        mockEntry.setReview(reviews);
        entries.add(mockEntry);
        scores.put(mockEntry, (double) mockReview.getScore());
        Mockito.when(reviewMockService.getEntryFinalScore(mockEntry)).thenReturn((double) mockReview.getScore());

        // Act
        entryService.getFinalScoresForEntriesList(entries);

        // Assert
        Mockito.verify(reviewMockService, Mockito.times(1)).getEntryFinalScore(mockEntry);
        Assertions.assertEquals(scores, entryService.getFinalScoresForEntriesList(entries));
    }

    @Test
    public void getWinnerOrWinners_Return_Empty_Map_WhenThereAreNoEntryForContest() {
        Map<Integer, List<Entry>> winners = new HashMap<>();
        List<Entry> allEntries = new ArrayList<>();
        Contest mockContest = createMockOpenContest();
        Mockito.when(mockRepository.getAllByContestId(mockContest.getId())).thenThrow(EntityNotFoundException.class);

        //Act
        Map<Integer, List<Entry>> result = entryService.getWinnerOrWinners(mockContest);

        // Assert
        Assertions.assertEquals(result.size(), 0);
    }

    @Test
    public void getWinnerOrWinners_should_Return_MapWithWinners_WhenInputValid() {
        // Arrange
        List<Entry> mockEntries = new ArrayList<>();
        Contest mockContest = createMockOpenContest();
        Entry mockEntryOne = createMockEntry();
        Entry mockEntryTwo = createMockEntry();
        mockEntries.add(mockEntryOne);
        mockEntries.add(mockEntryTwo);
        Mockito.when(entryService.getAllByContestId(mockContest.getId()))
                .thenReturn(mockEntries);
        Mockito.when(reviewMockRepository.avgEntryScore(mockEntryOne)).thenReturn(10.0);
        Mockito.when(reviewMockRepository.avgEntryScore(mockEntryTwo)).thenReturn(7.0);

        //Act
        // Assert
        Assertions.assertEquals(2,entryService.getWinnerOrWinners(mockContest).size());
    }

    @Test
    public void getAllWinners_should_Return_MapWithWinners_WhenInputValid() {
        // Arrange
        List<Entry> mockEntries = new ArrayList<>();
        List<Contest> mockFinishedContests = new ArrayList<>();
        Contest mockContest = createMockOpenContest();
        mockFinishedContests.add(mockContest);
        Entry mockEntryOne = createMockEntry();
        Entry mockEntryTwo = createMockEntry();
        Entry mockEntryThree = createMockEntry();
        Entry mockEntryFour = createMockEntry();
        User mockUserOne = mockEntryOne.getParticipant();
        mockEntryTwo.getParticipant().setId(2);
        User mockUserTwo = mockEntryTwo.getParticipant();
        mockEntryThree.getParticipant().setId(3);
        User mockUserThree = mockEntryThree.getParticipant();
        mockEntryFour.getParticipant().setId(4);
        User mockUserFour = mockEntryFour.getParticipant();

        mockEntries.add(mockEntryOne);
        mockEntries.add(mockEntryTwo);
        mockEntries.add(mockEntryThree);
        mockEntries.add(mockEntryFour);

        Mockito.when(entryService.getAllByContestId(mockContest.getId()))
                .thenReturn(mockEntries);
        Mockito.when(reviewMockRepository.avgEntryScore(mockEntryOne)).thenReturn(10.0);
        Mockito.when(reviewMockRepository.avgEntryScore(mockEntryTwo)).thenReturn(7.0);
        Mockito.when(reviewMockRepository.avgEntryScore(mockEntryThree)).thenReturn(5.0);
        Mockito.when(userMockRepository.getById(mockEntryFour.getParticipant().getId())).thenReturn(mockUserFour);

        //Act
        // Assert
        Assertions.assertEquals(1,entryService.getAllWinners(mockFinishedContests).size());
    }

    @Test
    public void getWinnerOrWinners_should_call_UserRepository_WhenInputValid() {
        // Arrange
        List<Entry> mockEntries = new ArrayList<>();
        Contest mockContest = createMockOpenContest();
        Entry mockEntryOne = createMockEntry();
        Entry mockEntryTwo = createMockEntry();
        Entry mockEntryThree = createMockEntry();
        Entry mockEntryFour = createMockEntry();
        User mockUserOne = mockEntryOne.getParticipant();
        mockEntryTwo.getParticipant().setId(2);
        User mockUserTwo = mockEntryTwo.getParticipant();
        mockEntryThree.getParticipant().setId(3);
        User mockUserThree = mockEntryThree.getParticipant();
        mockEntryFour.getParticipant().setId(4);
        User mockUserFour = mockEntryFour.getParticipant();

        mockEntries.add(mockEntryOne);
        mockEntries.add(mockEntryTwo);
        mockEntries.add(mockEntryThree);
        mockEntries.add(mockEntryFour);

        Mockito.when(entryService.getAllByContestId(mockContest.getId()))
                .thenReturn(mockEntries);
        Mockito.when(reviewMockRepository.avgEntryScore(mockEntryOne)).thenReturn(10.0);
        Mockito.when(reviewMockRepository.avgEntryScore(mockEntryTwo)).thenReturn(7.0);
        Mockito.when(reviewMockRepository.avgEntryScore(mockEntryThree)).thenReturn(5.0);
        Mockito.when(reviewMockRepository.avgEntryScore(mockEntryFour)).thenReturn(3.0);
        Mockito.when(userMockRepository.getById(mockEntryOne.getParticipant().getId())).thenReturn(mockUserOne);
        Mockito.when(userMockRepository.getById(mockEntryTwo.getParticipant().getId())).thenReturn(mockUserTwo);
        Mockito.when(userMockRepository.getById(mockEntryThree.getParticipant().getId())).thenReturn(mockUserThree);
        Mockito.when(userMockRepository.getById(mockEntryFour.getParticipant().getId())).thenReturn(mockUserFour);

        //Act
        entryService.calculateWinners(mockContest);

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).update(mockUserFour);
    }

    @Test
    public void calculateWinners_should_call_UserRepository_WhenInputValid() {
        // Arrange
        List<Entry> mockEntries = new ArrayList<>();
        Contest mockContest = createMockOpenContest();
        Entry mockEntryOne = createMockEntry();
        Entry mockEntryTwo = createMockEntry();
        User mockUserOne = mockEntryOne.getParticipant();
        mockEntryTwo.getParticipant().setId(2);
        User mockUserTwo = mockEntryTwo.getParticipant();
        mockEntries.add(mockEntryOne);
        mockEntries.add(mockEntryTwo);
        Mockito.when(entryService.getAllByContestId(mockContest.getId()))
                .thenReturn(mockEntries);
        Mockito.when(reviewMockRepository.avgEntryScore(mockEntryOne)).thenReturn(10.0);
        Mockito.when(reviewMockRepository.avgEntryScore(mockEntryTwo)).thenReturn(7.0);
        Mockito.when(userMockRepository.getById(mockEntryOne.getParticipant().getId())).thenReturn(mockUserOne);
        Mockito.when(userMockRepository.getById(mockEntryTwo.getParticipant().getId())).thenReturn(mockUserTwo);

        //Act
        entryService.calculateWinners(mockContest);

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).update(mockUserOne);
        Mockito.verify(userMockRepository, Mockito.times(1)).update(mockUserTwo);
    }
}

