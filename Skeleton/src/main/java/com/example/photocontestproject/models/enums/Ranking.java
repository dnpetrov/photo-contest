package com.example.photocontestproject.models.enums;

public enum Ranking {

    JUNKIE,
    ENTHUSIAST,
    MASTER,
    DICTATOR;

    @Override
    public String toString() {
        switch (this) {
            case JUNKIE:
                return "Junkie";
            case ENTHUSIAST:
                return "Enthusiast";
            case MASTER:
                return "Master";
            case DICTATOR:
                return "Wise and Benevolent Photo Dictator";
            default:
                throw new IllegalArgumentException();
        }
    }
}
