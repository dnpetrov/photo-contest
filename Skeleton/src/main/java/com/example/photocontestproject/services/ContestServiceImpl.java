package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Phase;
import com.example.photocontestproject.repositories.contracts.ContestRepository;
import com.example.photocontestproject.services.contracts.ContestService;
import com.example.photocontestproject.services.contracts.EntryService;
import com.example.photocontestproject.services.contracts.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.photocontestproject.utils.constants.GlobalConstants.*;

@Service
public class ContestServiceImpl implements ContestService {

    private final ContestRepository contestRepository;
    private final EntryService entryService;
    private final ReviewService reviewService;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository, EntryService entryService, ReviewService reviewService) {
        this.contestRepository = contestRepository;
        this.entryService = entryService;
        this.reviewService = reviewService;
    }

    @Override
    public Contest getById(int id) {
        try {
            return contestRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public Contest getByTitle(String title) {
        try {
            return contestRepository.getByField("title", title);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public List<Contest> getOpenByUser(User user) {
        try {
            return contestRepository.getOpenByUser(user);
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contest> getFinishedByUser(User user) {
        try {
            return contestRepository.getFinishedByUser(user);
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contest> getAllPhaseOne(int maxResults, User loggedUser) {
        try {
            if (isOrganizer(loggedUser)) {
                return contestRepository.getAllInPhaseWithOrWithoutInvites(0, Phase.FIRST, maxResults);
            } else {
                return contestRepository.getAllInPhaseWithOrWithoutInvites(loggedUser.getId(), Phase.FIRST, maxResults);
            }
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contest> getAllPhaseTwo(int maxResults, User loggedUser) {
        try {
            if (isOrganizer(loggedUser)) {
                return contestRepository.getAllInPhaseWithOrWithoutInvites(0, Phase.SECOND, maxResults);
            } else {
                return contestRepository.getAllInPhaseWithOrWithoutInvites(loggedUser.getId(), Phase.SECOND, maxResults);
            }
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contest> getAllPhaseFinished(int maxResults, User loggedUser) {
        try {
            if (isOrganizer(loggedUser)) {
                return contestRepository.getAllInPhaseWithOrWithoutInvites(0, Phase.FINISHED, maxResults);
            } else {
                return contestRepository.getAllInPhaseWithOrWithoutInvites(loggedUser.getId(), Phase.FINISHED, maxResults);
            }
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contest> getMostRecentlyFinished() {
        try {
            return contestRepository.getMostRecentlyFinished();
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contest> getAllNotFinished(User loggedUser) {
        List<Contest> contests = new ArrayList<>();
        contests.addAll(getAllPhaseOne(0,loggedUser));
        contests.addAll(getAllPhaseTwo(0,loggedUser));
        return contests;
    }

    private List<Contest> getAllNotFinished() {
        try {
            return contestRepository.getAllNotFinished();
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Contest> getAll() {
        try {
            return contestRepository.getAll();
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public int getCountAllContests() {
        return contestRepository.getCountAllContests();
    }

    @Override
    public void create(Contest contest, User loggedUser) {
        if (!loggedUser.getRole().getRole().equals(ORGANIZER_ROLE)) {
            throw new UnauthorizedOperationException(ONLY_USERS_WITH_ORGANIZER_ROLE_COULD_CREATE_CONTESTS);
        }
        boolean duplicateExists = true;
        try {
            getByTitle(contest.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException(CONTEST_UPPERCASE, TITLE_LOWERCASE, contest.getTitle());
        }
        contestRepository.create(contest);
    }

    @Override
    public void update(Contest contest, User loggedUser) {
        if (!loggedUser.getRole().getRole().equals(ORGANIZER_ROLE)) {
            throw new UnauthorizedOperationException(ONLY_USERS_WITH_ORGANIZER_ROLE_COULD_CREATE_CONTESTS);
        }
        try {
            contestRepository.getById(contest.getId());
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        contestRepository.update(contest);
    }

    @Override
    public void delete(int id, User loggedUser) {
        if (!loggedUser.getRole().getRole().equals(ORGANIZER_ROLE)) {
            throw new UnauthorizedOperationException(ONLY_USERS_WITH_ORGANIZER_ROLE_COULD_CREATE_CONTESTS);
        }
        contestRepository.delete(id);
    }

    @Override
    public void progressPhase() {
        for (Contest contest : getAllNotFinished()) {
            if (contest.getPhase().equals(Phase.FIRST)
                    && contest.getPhaseOneExpiration().isBefore(LocalDateTime.now())) {
                contest.setPhase(Phase.SECOND);
                contestRepository.update(contest);
                System.out.println(contest.getTitle() + CHANGED_PHASE_TWO);
            }
            if (contest.getPhaseTwoExpiration().isBefore(LocalDateTime.now())) {
                contest.setPhase(Phase.FINISHED);
                contestRepository.update(contest);
                createDefaultReviewsIfNecessary(contest);
                entryService.calculateWinners(contest);
                System.out.println(contest.getTitle() + CHANGED_FINISHED);
            }
        }
        System.out.println(CONTEST_CHECKED + LocalDateTime.now());
    }

    @Override
    public long calculatePhaseTimeDiff(Contest contest) {
        LocalDateTime timeNow = LocalDateTime.now();
        LocalDateTime timeFuture;
        long timeDifference;

        if (contest.getPhase().equals(Phase.FIRST)) {
            timeFuture = contest.getPhaseOneExpiration();
        } else {
            timeFuture = contest.getPhaseTwoExpiration();
        }
        timeDifference = ChronoUnit.SECONDS.between(timeNow, timeFuture);
        return timeDifference;
    }

    @Override
    public List<Contest> filter(Optional<String> title, Optional<String> openAccess,
                                Optional<Phase> phase, Optional<Integer> categoryId) {
        return contestRepository.filter(title, openAccess, phase, categoryId);
    }

    private void createDefaultReviewsIfNecessary(Contest contest) {
        for (Entry entry : entryService.getAllByContestId(contest.getId())) {
            reviewService.getEntryFinalScore(entry);
        }
    }

    private boolean isOrganizer(User loggedUser) {
        return loggedUser.getRole().getRole().equals(ORGANIZER_ROLE);
    }
}

