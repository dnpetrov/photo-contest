package com.example.photocontestproject.services.contracts;

import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface EntryService {

    List<Entry> getAll();

    List<Entry> getAllForOpenContests();

    List<Entry> getAllByContestId(int id);

    List<Entry> getAllByUser(User user);

    Entry getById(int id);

    Entry getByTitle(String title);

    Entry getByUserAndContestId(User user, int contestId);

    HashMap<Entry, Double> getFinalScoresForEntriesList(List<Entry> entries);

    int getCountByContestId(int id);

    int getCountAllEntries();

    void create(Entry entry, User loggedUser);

    void update(Entry entry, User loggedUser);

    void delete(int id, User loggedUser);

    Map<Integer,List<Entry>> getWinnerOrWinners(Contest contest);

    Map<Contest,Map<Integer,List<Entry>>> getAllWinners(List<Contest> finished);

    void calculateWinners(Contest contest);

    List<Entry> search(Optional<String> search, User loggedUser);
}
