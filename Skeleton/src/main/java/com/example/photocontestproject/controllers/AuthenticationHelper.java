package com.example.photocontestproject.controllers;

import com.example.photocontestproject.exceptions.AuthenticationFailureException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

import static com.example.photocontestproject.utils.constants.GlobalConstants.*;

@Component
public class AuthenticationHelper {

    private final UserService service;

    @Autowired
    public AuthenticationHelper(UserService service) {
        this.service = service;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_ERROR_MESSAGE);
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return service.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_USERNAME_ERROR_MESSAGE);
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute(CURRENT_USER);

        if (currentUser == null) {
            throw new AuthenticationFailureException(USER_LOGGED_IN_ERROR_MESSAGE);
        }

        return service.getByUsername(currentUser);
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = service.getByUsername(username);
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }

    public User verifyUserProfile(HttpSession session, int id) {
        User user = tryGetUser(session);
        if (!user.getRole().getRole().equals(ORGANIZER_ROLE) && user.getId() != id)
            throw new UnauthorizedOperationException(USER_VIEW_ERROR_MESSAGE);
        return user;
    }

    public User verifyOrganizerAuthorization(HttpSession session) {
        User user = tryGetUser(session);
        if (!user.getRole().getRole().equals(ORGANIZER_ROLE))
            throw new UnauthorizedOperationException(INFORMATION_ERROR_MESSAGE);
        return user;
    }

    public User verifyUpdateUser(HttpSession session, int id) {
        User user = tryGetUser(session);
        if (user.getId() != id)
            throw new UnauthorizedOperationException(USER_EDIT_ERROR_MESSAGE);
        return user;
    }
}

