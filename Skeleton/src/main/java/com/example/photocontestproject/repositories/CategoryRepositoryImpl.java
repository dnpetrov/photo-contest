package com.example.photocontestproject.repositories;

import com.example.photocontestproject.models.Category;
import com.example.photocontestproject.repositories.contracts.CategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryRepositoryImpl extends AbstractCRUDRepository<Category> implements CategoryRepository {

    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(Category.class, sessionFactory);
    }
}
