package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Category;
import com.example.photocontestproject.repositories.contracts.CategoryRepository;
import com.example.photocontestproject.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }

    @Override
    public Category getById(int id) {
        return categoryRepository.getById(id);
    }

    @Override
    public Category getByName(String name) {
        Category category;
        try {
            category = categoryRepository.getByField("name",name);
        } catch (EntityNotFoundException e) {
            category = new Category(0,name);
            categoryRepository.create(category);
        }
        return categoryRepository.getByField("name",name);
    }

    @Override
    public void create(Category category) {
        boolean duplicateExists = true;
        try {
            getByName(category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }
        categoryRepository.create(category);
    }
}
