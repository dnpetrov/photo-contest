USE photo;

/*!40000 ALTER TABLE `categories`
    DISABLE KEYS */;
INSERT INTO `categories` (`category_id`, `name`)
VALUES (4, 'Black and white'),
       (6, 'General'),
       (1, 'Landscape'),
       (2, 'Portrait'),
       (5, 'Urban'),
       (3, 'Wild life');
/*!40000 ALTER TABLE `categories`
    ENABLE KEYS */;

/*!40000 ALTER TABLE `contests`
    DISABLE KEYS */;
INSERT INTO `contests` (`contest_id`, `title`, `category_id`, `open_access`, `creation_time`, `phase`,
                        `phase_one_expiration`, `phase_two_expiration`)
VALUES
    (10, 'Beautiful Landscape Views', 1, 1, '2022-04-12 09:11:04', 1, '2022-04-19 09:11:04', '2022-04-30 18:11:04'),
    (1, 'City Urban Scenes', 5, 1, '2022-04-12 09:11:46', 0, '2022-04-30 09:11:46', '2022-04-30 17:11:46'),
    (2, 'People portraits', 2, 1, '2022-04-12 09:12:44', 1, '2022-04-13 09:12:44', '2022-04-30 01:12:44'),
    (3, 'Animals', 3, 1, '2022-04-12 09:13:13', 0, '2022-04-30 09:13:13', '2022-04-30 19:13:13'),
    (4, 'Sofia City', 5, 1, '2022-04-12 09:14:41', 0, '2022-04-30 09:14:41', '2022-04-30 21:14:41'),
    (5, 'No Color Masterpieces', 4, 1, '2022-04-12 09:15:30', 1, '2022-04-12 09:15:30', '2022-04-30 05:15:30'),
    (6, 'General Photo Masterpieces', 6, 1, '2022-04-12 09:16:12', 2, '2022-04-12 09:16:12', '2022-04-12 10:16:12'),
    (7, 'Explosive Creativity', 6, 1, '2022-04-12 09:16:47', 2, '2022-04-12 09:16:47', '2022-04-12 11:16:47'),
    (8, 'High Contrast - Invites only', 6, 0, '2022-04-12 11:18:21', 0, '2022-04-30 11:18:21', '2022-04-30 12:18:21'),
    (9, 'Best of Black and White', 4, 1, '2022-04-16 11:11:45', 2, '2022-04-16 11:16:45', '2022-04-16 11:36:45'),
    (11,'City Center - Invites only',5,0,'2022-04-16 18:04:13',0,'2022-04-30 18:04:13','2022-04-30 19:04:13');

/*!40000 ALTER TABLE `contests`
    ENABLE KEYS */;

/*!40000 ALTER TABLE `photos`
    DISABLE KEYS */;
INSERT INTO `photos` (`photo_id`, `is_cover_photo`, `is_url`, `path`)
VALUES
    (9, 1, 0, '/images/upload/cover/cover1.jpg'),
    (10, 1, 0, '/images/upload/cover/IMG_20171210_080505-PANO.jpg'),
    (11, 1, 0, '/images/upload/cover/ASK_1841-1.jpg'),
    (12, 1, 0, '/images/upload/cover/Lion.jpg'),
    (13, 1, 0, '/images/upload/cover/PeopleOfSofia.jpeg'),
    (14, 1, 0, '/images/upload/cover/photoshop-initial-black-white-conversion.jpg'),
    (15, 1, 0, '/images/upload/cover/20190914-DSC_1547-2.jpg'),
    (16, 1, 0, '/images/upload/cover/_DSC1893-1.jpg'),
    (18, 0, 0, '/images/upload/_DSC0941-HDR-1.jpg'),
    (19, 0, 0, '/images/upload/_DSC0881-1.jpg'),
    (20, 0, 0, '/images/upload/20200202-_DSC4611-2.jpg'),
    (21, 0, 0, '/images/upload/20200804-_DSC7739-12.jpg'),
    (22, 0, 0, '/images/upload/ASK_1842-1-2.jpg'),
    (23, 0, 0, '/images/upload/ASK_1567-1.jpg'),
    (24, 0, 0, '/images/upload/The door.jpg'),
    (25, 0, 0, '/images/upload/DSC35HDR1.jpg'),
    (26, 0, 0, '/images/upload/20200724-_DSC7310-1.jpg'),
    (27, 0, 0, '/images/upload/IMG22.jpg'),
    (28, 0, 0, '/images/upload/BackgroundLogin.jpeg'),
    (29, 0, 0, '/images/upload/ASK_1834-1.jpg'),
    (30, 0, 0, '/images/upload/ASK_1619-1.jpg'),
    (31, 0, 0, '/images/upload/IMG_20211003_193432.jpg'),
    (32, 0, 0, '/images/upload/images.jpg'),
    (33, 0, 0, '/images/upload/ASK_1547-1.jpg'),
    (34, 1, 0, '/images/upload/cover/_DSC9481-10.jpg'),
    (35, 0, 0, '/images/upload/_DSC8143-1.jpg'),
    (36, 0, 0, '/images/upload/20200718-_DSC6771-1.jpg'),
    (37, 0, 0, '/images/upload/ASK_0027-4.jpg'),
    (38, 0, 0, '/images/upload/ASK_1639-1-3.jpg'),
    (39, 0, 0, '/images/upload/IMG_20210731_114354-EFFECTS.jpg'),
    (40, 1, 0, '/images/upload/cover/ASK_9437-1.jpg'),
    (44, 0, 0, '/images/upload/shadow-tree-g51909ec58_1920.jpg'),
    (45, 1, 0, '/images/upload/cover/art-gba60180fd_1920.jpg')  ;
/*!40000 ALTER TABLE `photos`
    ENABLE KEYS */;

/*!40000 ALTER TABLE `contests_photos`
    DISABLE KEYS */;
INSERT INTO `contests_photos` (`cover_photo_id`, `contest_id`, `photo_id`)
VALUES
    (10, 10, 9),
    (11, 1, 10),
    (12, 2, 11),
    (13, 3, 12),
    (14, 4, 13),
    (15, 5, 45),
    (16, 6, 15),
    (17, 7, 16),
    (18, 8, 34),
    (19, 9, 14),
    (20, 11, 40);
/*!40000 ALTER TABLE `contests_photos`
    ENABLE KEYS */;

/*!40000 ALTER TABLE `roles`
    DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role`)
VALUES (1, 'Organizer'),
       (2, 'User');
/*!40000 ALTER TABLE `roles`
    ENABLE KEYS */;

/*!40000 ALTER TABLE `users`
    DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `username`, `password`, `email`, `points`, `ranking`,
                     `creation_time`, `role_id`)
VALUES (1, 'Yoana', 'Stoeva', 'stoevay', 'pass01', 'ystoeva@gmail.com', 2000, 3, '2022-02-15 00:00:00', 1),
       (2, 'Dimitar', 'Petrov', 'petrovd', 'pass02', 'dpetrov@abv.bg', 2000, 3, '2022-02-15 00:00:00', 1),
       (3, 'Denislav', 'Vuchkov', 'vuchkovd', 'pass03', 'dvuchkov@gmail.com', 131, 1, '2022-02-15 00:00:00', 2),
       (4, 'Lilia', 'Georgieva', 'georgieval', 'pass04', 'lgeorgieva@gmail.com', 36, 0, '2022-02-15 00:00:00', 2),
       (5, 'Svetoslav', 'Hristov', 'hristovs', 'pass05', 'shristov@yahoomail.com', 49, 0, '2022-02-15 00:00:00', 2),
       (6, 'Georgi', 'Dinkov', 'dinkovg', 'pass06', 'gdinkov@gmail.com', 0, 0, '2022-02-15 00:00:00', 2),
       (7, 'Tihomir', 'Dimitrov', 'dimitrovt', 'pass07', 'tdimitrov@gmail.com', 21, 0, '2022-02-15 00:00:00', 2),
       (8, 'Georgi', 'Stunkanyov', 'stunkanyovd', 'pass08', 'gstunkanyov@gmail.com', 0, 0, '2022-02-15 00:00:00', 2),
       (9, 'Asen', 'Monov', 'monova', 'pass09', 'amonov@abv.bg', 88, 1, '2022-02-15 00:00:00', 2),
       (10, 'Will', 'Smith', 'smithw', 'pass10', 'wsmith@yahoomail.com', 1200, 3, '2022-02-15 00:00:00', 2),
       (11, 'Yana', 'Petrova', 'petrovay', 'pass11', 'ypetrova@abv.bg', 950, 2, '2022-02-15 00:00:00', 2),
       (12, 'Dimitar', 'Petkov', 'petkovd', 'pass12', 'dpetkov@abv.bg', 0, 0, '2022-02-15 00:00:00', 2),
       (13, 'Stoycho', 'Bosev', 'bosevs', 'pass13', 'sbosev@abv.bg', 750, 2, '2022-02-15 00:00:00', 2),
       (14, 'Victor', 'Petrov', 'petrovv', 'pass14', 'vpetrov@abv.bg', 1200, 3, '2022-02-15 00:00:00', 2),
       (15, 'Stefan', 'Subev', 'subevs', 'pass15', 'ssubev@abv.bg', 920, 2, '2022-02-15 00:00:00', 2),
       (16, 'Yanko', 'Nikolov', 'nikolovy', 'pass16', 'ynikolov@abv.bg', 87, 1, '2022-02-15 00:00:00', 2),
       (17, 'Valentina', 'Petrova', 'petrovav', 'pass17', 'vpetrova@gmail.com', 0, 0, '2022-02-15 00:00:00', 2),
       (18, 'Severina', 'Haralampieva', 'sevi', 'pass18', 'shara@gmail.com', 0, 0, '2022-02-15 00:00:00', 2),
       (19, 'Stefan', 'Pavlov', 'pavlovs', 'pass19', 'pavlovs@yahoo.com', 1, 0, '2022-02-15 00:00:00', 2);
/*!40000 ALTER TABLE `users`
    ENABLE KEYS */;


/*!40000 ALTER TABLE `entries`
    DISABLE KEYS */;
INSERT INTO `entries` (`entry_id`, `contest_id`, `title`, `story`, `is_invited`, `user_id`, `photo_id`)
VALUES
    (9, 10, 'The Triangle Rock', 'View from above', 0, 3, 18),
    (10, 10, 'Mountain', 'Houses on the mountain', 0, 4, 19),
    (11, 1, 'Citry center', 'Busy city center', 0, 5, 20),
    (12, 2, 'Night portrait', 'In the darkness', 0, 5, 21),
    (13, 5, 'Portrait', 'Black and white portrait', 0, 5, 22),
    (14, 7, 'Marteniza', 'Red and white colors', 0, 9, 23),
    (15, 6, 'The door', 'Light from ancient door', 0, 9, 24),
    (16, 10, 'Sveta Petka', 'Sunlight over mountain village ', 0, 16, 25),
    (17, 6, 'Sunset', 'See sunset', 0, 16, 26),
    (18, 1, 'Paris', 'City of Paris', 0, 16, 27),
    (19, 7, 'Car', 'Nice photo of a great car', 0, 16, 28),
    (20, 2, 'Kid', 'A photo of a kid in a nice background', 0, 16, 29),
    (21, 10, 'Vitosha', 'Lift on the mountain Vitosha', 0, 18, 30),
    (22, 1, 'Paris Louvre', 'The Louvre building in Paris', 0, 18, 31),
    (23, 5, 'Mystic River', 'Black and white mystic river', 0, 18, 32),
    (24, 2, 'Small girl playing', 'Hobby photography', 0, 18, 33),
    (25, 9, 'Beautiful Flower', 'Red contrast on B&W photo', 0, 3, 35),
    (26, 9, 'Car', 'B&W composition of a car view', 0, 4, 36),
    (27, 9, 'Anonymous', 'Capture of the anonymous magician ', 0, 5, 37),
    (28, 9, 'Father and son', 'Family photo in the mountain', 0, 7, 38),
    (29, 9, 'Mountain view', 'A view from the top with no colors', 0, 9, 39),
    (33, 9, 'Night tree', 'Captured in a mystery evening with a full moon', 0, 19, 44);
/*!40000 ALTER TABLE `entries`
    ENABLE KEYS */;

/*!40000 ALTER TABLE `reviews`
    DISABLE KEYS */;
INSERT INTO `reviews` (`review_id`, `score`, `comment`, is_wrong_category, `juror_id`, `entry_id`)
VALUES
    (20, 8, 'Very sweet picture!', 0, 2, 20),
    (1,9, 'Traditions!Very nicte capture', 0, 1, 14),
    (2,10, 'Creative idea!', 0, 2, 14),
    (3,7, 'This is a nice creative idea.', 0, 1, 15),
    (4,8, 'Very good picture', 0, 2, 15),
    (5,10, 'Top notch photo!', 0, 1, 17),
    (6,10, 'Beautiful sunset photo!', 0, 2, 17),
    (7,9, 'Beautiful!', 0, 1, 19),
    (8,8, 'Luxury. Cool photo!', 0, 2, 19),
    (28, 8, 'Great photo', 0, 2, 23),
    (44, 10, 'The best', 0, 1, 25),
    (45, 8, 'Good one', 0, 1, 26),
    (46, 10, 'Great detail', 0, 1, 27),
    (47, 5, 'cool', 0, 1, 28),
    (48, 1, 'Brightness is bad ', 0, 1, 29),
    (49, 10, 'Incredible photo!', 0, 2, 25),
    (50, 10, 'Great portrait', 0, 2, 27),
    (51, 6, 'nice one', 0, 2, 26),
    (52, 7, 'Cool', 0, 2, 28),
    (53, 2, 'bad one', 0, 2, 29),
    (54, 2, 'Not very original', 0, 2, 33),
    (55, 2, 'Not good enough for a contest photo ', 0, 1, 33);
/*!40000 ALTER TABLE `reviews`
    ENABLE KEYS */;

/*!40000 ALTER TABLE `users_contests`
    DISABLE KEYS */;
INSERT INTO `users_contests` (`jury_id`, `contest_id`, `user_id`)
VALUES
    (27, 10, 1),
    (28, 10, 2),
    (1, 10, 10),
    (29, 1, 1),
    (30, 1, 2),
    (31, 2, 1),
    (32, 2, 2),
    (2, 2, 10),
    (33, 3, 1),
    (34, 3, 2),
    (35, 3, 14),
    (36, 3, 13),
    (37, 4, 1),
    (38, 4, 2),
    (39, 4, 11),
    (40, 5, 1),
    (41, 5, 2),
    (42, 5, 10),
    (43, 6, 1),
    (44, 6, 2),
    (46, 7, 1),
    (47, 7, 2),
    (48, 8, 1),
    (49, 8, 2),
    (50, 8, 13),
    (51, 8, 14),
    (52, 9, 1),
    (53, 9, 2),
    (54, 11, 1),
    (55, 11, 2);
/*!40000 ALTER TABLE `users_contests`
    ENABLE KEYS */;