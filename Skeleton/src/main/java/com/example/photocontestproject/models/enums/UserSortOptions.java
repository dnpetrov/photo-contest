package com.example.photocontestproject.models.enums;

import java.util.HashMap;
import java.util.Map;

public enum UserSortOptions {
    NAME_ASC("First name: A-Z", " order by first_name "),
    NAME_DESC("First name: Z-A", " order by first_name desc "),
    POINTS_ASC("Score: Lowest First", " order by points "),
    POINTS_DESC("Score: Highest First", " order by points desc ");

    private final String preview;
    private final String query;
    private static final Map<String, UserSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (UserSortOptions option : values()) {
            BY_PREVIEW.put(option.getPreview(), option);
        }
    }

    UserSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static UserSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}
