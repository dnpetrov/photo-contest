package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.IllegalRequestException;
import com.example.photocontestproject.exceptions.InvalidContestPhaseException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Review;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Phase;
import com.example.photocontestproject.repositories.contracts.EntryRepository;
import com.example.photocontestproject.repositories.contracts.ReviewRepository;
import com.example.photocontestproject.services.contracts.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.example.photocontestproject.utils.constants.GlobalConstants.*;

@Service
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository reviewRepository;
    private final EntryRepository entryRepository;

    @Autowired
    public ReviewServiceImpl(ReviewRepository reviewRepository, EntryRepository entryRepository) {
        this.reviewRepository = reviewRepository;
        this.entryRepository = entryRepository;
    }

    @Override
    public List<Review> getAll() {
        return reviewRepository.getAll();
    }

    @Override
    public int getCountAllReviews() {
        return reviewRepository.getCountAllReviews();
    }

    @Override
    public Review getById(int id) {
        return reviewRepository.getById(id);
    }

    @Override
    public List<Review> getByEntry(Entry entry) {
        return reviewRepository.getByEntry(entry.getId());
    }

    @Override
    public void create(Review review, User juror) {
        Entry entry = review.getEntry();
        checkContestPhase(entry.getContest(), Phase.SECOND, SECOND_PHASE_ERROR_MESSAGE);
        ensureUnique(juror.getId(), entry.getId());
        if (review.getScore() < 0 || review.getScore() > 10) {
            throw new IllegalRequestException("Score must be in the interval 1-10");
        }
        if (review.getComment().isEmpty()) {
            throw new IllegalRequestException("Comment field can not be empty");
        }
        reviewRepository.create(review);
        entry.createReview(review);
    }

    @Override
    public double getEntryFinalScore(Entry entry) {
        checkContestPhase(entry.getContest(), Phase.FINISHED, FINISHED_PHASE_ERROR_MESSAGE);
        if (entry.getContest().getJury().size() > reviewRepository.getEntryReviewCount(entry.getId())) {
            List<Review> currentEntryReviews = reviewRepository.getByEntry(entry.getId());
            List<User> reviewers = new ArrayList<>();
            if (!currentEntryReviews.isEmpty()) {
                for (Review review : currentEntryReviews) {
                    reviewers.add(review.getJuror());
                }
            }
            for (User juror : entry.getContest().getJury()) {
                if (!reviewers.contains(juror)) {
                    Review defaultReview = new Review();
                    defaultReview.setEntry(entry);
                    defaultReview.setJuror(juror);
                    defaultReview.setScore(DEFAULT_SCORE);
                    defaultReview.setComment(DEFAULT_COMMENT);
                    reviewRepository.create(defaultReview);
                }
            }
        }
        return reviewRepository.avgEntryScore(entry);
    }

    private void ensureUnique(int jurorId, int entryId) {
        try {
            reviewRepository.getByJurorAndByEntry(jurorId, entryId);
            throw new UnauthorizedOperationException(JUROR_ALREADY_REVIEWED_ERROR_MESSAGE);
        } catch (EntityNotFoundException e) {
            new Review();
        }
    }

    private void checkContestPhase(Contest contest, Phase phase, String errorMessage) {
        if (!contest.getPhase().equals(phase)) {
            throw new InvalidContestPhaseException(errorMessage);
        }
    }
}
