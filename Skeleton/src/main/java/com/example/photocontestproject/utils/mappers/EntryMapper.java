package com.example.photocontestproject.utils.mappers;

import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Photo;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.dtos.EntryDto;
import com.example.photocontestproject.services.contracts.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EntryMapper {

    private final ContestService contestService;
    @Autowired
    public EntryMapper( ContestService contestService) {
        this.contestService = contestService;
    }

    public Entry dtoToObject(EntryDto entryDto,Photo photo, int contestId, User user) {
        Entry entry = new Entry();
        entry.setContest(contestService.getById(contestId));
        entry.setPhoto(photo);
        entry.setParticipant(user);
        entry.setTitle(entryDto.getTitle());
        entry.setStory(entryDto.getStory());
        entry.setInvited(false);
        return entry;
    }

    public Entry dtoToObject(EntryDto entryDto) {
        Entry entry = new Entry();
        entry.setContest(contestService.getById(entryDto.getContestId()));
        entry.setTitle(entryDto.getTitle());
        entry.setStory(entryDto.getStory());
        entry.setInvited(false);
        entry.setPhoto(entryDto.getPhoto());
        return entry;
    }
}
