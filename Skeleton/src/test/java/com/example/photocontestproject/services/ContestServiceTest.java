package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Phase;
import com.example.photocontestproject.repositories.contracts.ContestRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.example.photocontestproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ContestServiceTest {

    @Mock
    ContestRepository mockRepository;

    @InjectMocks
    ContestServiceImpl contestService;

    @Test
    public void getAll_Should_ReturnAllContests_When_ContestExist() {
        //Arrange
        Mockito.when(mockRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        contestService.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_Should_Throw_When_ContestDoesNotExist() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenThrow(EntityNotFoundException.class);

        //Act
        contestService.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnContest_When_ContestExist() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        Mockito.when(mockRepository.getById(mockContest.getId())).thenReturn(null);

        //Act
        contestService.getById(mockContest.getId());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(mockContest.getId());
    }

    @Test
    public void getById_Should_Throw_When_ContestDoesNotExist() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        Mockito.when(mockRepository.getById(mockContest.getId()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.getById(mockContest.getId()));
    }

    @Test
    public void getByTitle_Should_ReturnContest_When_ContestExist() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        Mockito.when(mockRepository.getByField("title", mockContest.getTitle())).thenReturn(null);

        //Act
        contestService.getByTitle(mockContest.getTitle());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getByField("title", mockContest.getTitle());
    }

    @Test
    public void getByTitle_Should_Throw_When_ContestDoesNotExist() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        Mockito.when(mockRepository.getByField("title", mockContest.getTitle()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> contestService.getByTitle(mockContest.getTitle()));
    }

    @Test
    public void getOpenByUser_Should_ReturnContest_When_ContestExist() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getOpenByUser(mockUser)).thenReturn(null);

        //Act
        contestService.getOpenByUser(mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getOpenByUser(mockUser);
    }

    @Test
    public void getOpenByUser_Should_Throw_When_ContestDoesNotExist() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getOpenByUser(mockUser))
                .thenThrow(EntityNotFoundException.class);

        //Act
        contestService.getOpenByUser(mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getOpenByUser(mockUser);
    }

    @Test
    public void getAllFinishesByUser_Should_ReturnContest_When_ContestExist() {
        //Arrange;
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getFinishedByUser(mockUser)).thenReturn(null);

        //Act
        contestService.getFinishedByUser(mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getFinishedByUser(mockUser);
    }

    @Test
    public void getAllFinishesByUser_Should_Throw_When_ContestDoesNotExist() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getFinishedByUser(mockUser))
                .thenThrow(EntityNotFoundException.class);

        //Act
        contestService.getFinishedByUser(mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getFinishedByUser(mockUser);
    }

    @Test
    public void getAllPhaseOne_Should_ReturnContest_When_ContestExistAndOrganizer() {
        //Arrange;
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(0, Phase.FIRST,0))
                .thenReturn(null);

        //Act
        contestService.getAllPhaseOne(0,mockOrganizer);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllInPhaseWithOrWithoutInvites(0,Phase.FIRST,0);
    }

    @Test
    public void getAllPhaseOne_Should_ReturnContest_When_ContestExistAndUser() {
        //Arrange;
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(mockUser.getId(), Phase.FIRST,0))
                .thenReturn(null);

        //Act
        contestService.getAllPhaseOne(0,mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllInPhaseWithOrWithoutInvites(mockUser.getId(),Phase.FIRST,0);
    }

    @Test
    public void getAllPhaseOne_Should_returnEmptyList_When_ContestDoesNotExist() {
        //Arrange;
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(0,Phase.FIRST,0))
                .thenThrow(EntityNotFoundException.class);

        //Act
        contestService.getAllPhaseOne(0, mockOrganizer);

        //Assert
        Assertions.assertEquals(0,contestService.getAllPhaseOne(0,mockOrganizer).size());
    }

    @Test
    public void getAllPhaseTwo_Should_ReturnContest_When_ContestExistAndOrganizer() {
        //Arrange;
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(0, Phase.SECOND,0))
                .thenReturn(null);

        //Act
        contestService.getAllPhaseTwo(0,mockOrganizer);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllInPhaseWithOrWithoutInvites(0,Phase.SECOND,0);
    }

    @Test
    public void getAllPhaseTwo_Should_ReturnContest_When_ContestExistAndUser() {
        //Arrange;
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(mockUser.getId(), Phase.SECOND,0))
                .thenReturn(null);

        //Act
        contestService.getAllPhaseTwo(0,mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllInPhaseWithOrWithoutInvites(mockUser.getId(),Phase.SECOND,0);
    }

    @Test
    public void getAllPhaseTwo_Should_returnEmptyList_When_ContestDoesNotExist() {
        //Arrange;
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(0,Phase.SECOND,0))
                .thenThrow(EntityNotFoundException.class);

        //Act
        contestService.getAllPhaseTwo(0, mockOrganizer);

        //Assert
        Assertions.assertEquals(0,contestService.getAllPhaseTwo(0,mockOrganizer).size());
    }

    @Test
    public void getAllPhaseFinished_Should_ReturnContest_When_ContestExistAndOrganizer() {
        //Arrange;
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(0, Phase.FINISHED,0))
                .thenReturn(null);

        //Act
        contestService.getAllPhaseFinished(0,mockOrganizer);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllInPhaseWithOrWithoutInvites(0,Phase.FINISHED,0);
    }

    @Test
    public void getAllPhaseFinished_Should_ReturnContest_When_ContestExistAndUser() {
        //Arrange;
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(mockUser.getId(), Phase.FINISHED,0))
                .thenReturn(null);

        //Act
        contestService.getAllPhaseFinished(0,mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllInPhaseWithOrWithoutInvites(mockUser.getId(),Phase.FINISHED,0);
    }

    @Test
    public void getAllPhaseFinished_Should_returnEmptyList_When_ContestDoesNotExist() {
        //Arrange;
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(0,Phase.FINISHED,0))
                .thenThrow(EntityNotFoundException.class);

        //Act
        contestService.getAllPhaseFinished(0, mockOrganizer);

        //Assert
        Assertions.assertEquals(0,contestService.getAllPhaseFinished(0,mockOrganizer).size());
    }


    @Test
    public void getMostRecentlyFinished_Should_ReturnContest_When_ContestExist() {
        //Arrange;
        Mockito.when(mockRepository.getMostRecentlyFinished()).thenReturn(null);

        //Act
        contestService.getMostRecentlyFinished();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getMostRecentlyFinished();
    }

    @Test
    public void getMostRecentlyFinished_Throw_When_ContestDoesNotExist() {
        //Arrange;
        Mockito.when(mockRepository.getMostRecentlyFinished()).thenThrow(EntityNotFoundException.class);

        //Act
        contestService.getMostRecentlyFinished();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getMostRecentlyFinished();
    }

    @Test
    public void getAllNotFinished_Should_ReturnContest_When_ContestExist() {
        //Arrange;
        User mockUser = createMockOrganizer();
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(0,Phase.FIRST,0))
                .thenReturn(new ArrayList<>());
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(0,Phase.SECOND,0))
                .thenReturn(new ArrayList<>());

        //Act
        contestService.getAllNotFinished(mockUser);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllInPhaseWithOrWithoutInvites(0,Phase.FIRST,0);
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllInPhaseWithOrWithoutInvites(0,Phase.SECOND,0);
    }

    @Test
    public void getAllNotFinished_should_returnEmptyList_when_ContestDoesNotExist() {
        //Arrange;
        User mockUser = createMockOrganizer();
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(0,Phase.FIRST,0))
                .thenReturn(new ArrayList<>());
        Mockito.when(mockRepository.getAllInPhaseWithOrWithoutInvites(0,Phase.SECOND,0))
                .thenReturn(new ArrayList<>());

        //Act
        //Assert
        Assertions.assertEquals(0,contestService.getAllNotFinished(mockUser).size());
    }

    @Test
    public void getCountAll_Should_ReturnContest_When_ContestExist() {
        //Act
        contestService.getCountAllContests();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getCountAllContests();
    }

    @Test
    public void createContest_Should_CallRepository_When_ContestWithSameTitleDoesNotExists() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getByField("title", mockContest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        contestService.create(mockContest, mockOrganizer);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).create(mockContest);
    }

    @Test
    public void createContest_Should_Throw_When_ContestWithSameTitleExists() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        Contest mockContest2 = createMockOpenContest();
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getByField("title", mockContest.getTitle()))
                .thenReturn(mockContest);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> contestService.create(mockContest2, mockOrganizer));
    }

    @Test
    public void createContest_Should_Throw_When_UserIsNotOrganizer() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        User mockUser = createMockUser();

        //Act, Assert
        Mockito.verify(mockRepository, Mockito.times(0)).create(mockContest);
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> contestService.create(mockContest, mockUser));
    }

    @Test
    public void updateContest_Should_Throw_When_UserIsNotOrganizer() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        User mockUser = createMockUser();

        //Act, Assert
        Mockito.verify(mockRepository, Mockito.times(0)).update(mockContest);
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> contestService.update(mockContest, mockUser));
    }

    @Test
    public void updateContest_Should_Throw_When_ContestIdDoesNotExist() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getById(mockContest.getId()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> contestService.update(mockContest, mockOrganizer));
    }

    @Test
    public void updateContest_Should_CallRepository() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        User mockOrganizer = createMockOrganizer();
        Mockito.when(mockRepository.getById(mockContest.getId()))
                .thenReturn(mockContest);

        //Act
        contestService.update(mockContest, mockOrganizer);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getById(mockContest.getId());
        Mockito.verify(mockRepository, Mockito.times(1)).update(mockContest);
    }

    @Test
    public void deleteContest_Should_Throw_When_UserIsNotOrganizer() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        User mockUser = createMockUser();

        //Act, Assert
        Mockito.verify(mockRepository, Mockito.times(0)).delete(mockContest.getId());
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> contestService.delete(mockContest.getId(), mockUser));
    }

    @Test
    public void deleteContest_Should_CallRepository() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        User mockOrganizer = createMockOrganizer();

        //Act
        contestService.delete(mockContest.getId(), mockOrganizer);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).delete(mockContest.getId());
    }

    @Test
    public void filter_Should_Call_Repository_WhenSearchNotEmpty() {
        // Arrange
        Optional<String> title = Optional.of("mockTitle");
        Optional<String> openAccess = Optional.of("true");
        Optional<Phase> phase = Optional.of(Phase.FIRST);
        Optional<Integer> categoryId = Optional.of(1);
        Mockito.when(mockRepository.filter(title, openAccess, phase, categoryId))
                .thenReturn(new ArrayList<>());

        // Act
        contestService.filter(title, openAccess, phase, categoryId);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).filter(title, openAccess, phase, categoryId);
    }

    @Test
    public void filter_Should_Call_Repository_WhenSearchEmpty() {
        // Arrange
        Optional<String> title = Optional.empty();
        Optional<String> openAccess = Optional.of("true");
        Optional<Phase> phase = Optional.of(Phase.FIRST);
        Optional<Integer> categoryId = Optional.of(1);
        Mockito.when(mockRepository.filter(title, openAccess, phase, categoryId))
                .thenReturn(new ArrayList<>());

        // Act
        contestService.filter(title, openAccess, phase, categoryId);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).filter(title, openAccess, phase, categoryId);
    }

    @Test
    public void filter_Should_Call_Repository_WhenSearchNull() {
        // Arrange
        Optional<String> title = Optional.empty();
        Optional<String> openAccess = Optional.of("true");
        Optional<Phase> phase = Optional.of(Phase.FIRST);
        Optional<Integer> categoryId = Optional.of(1);
        Mockito.when(mockRepository.filter(title, openAccess, phase, categoryId))
                .thenReturn(new ArrayList<>());

        // Act
        contestService.filter(title, openAccess, phase, categoryId);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).filter(title, openAccess, phase, categoryId);
    }
}
