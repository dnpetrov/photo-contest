package com.example.photocontestproject.models;

import javax.persistence.*;

@Entity
@Table(name = "photos")
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_id")
    private int id;

    @Column(name = "is_cover_photo")
    private boolean isCoverPhoto;

    @Column(name = "is_url")
    private boolean isUrl;

    @Column(name = "path")
    private String path;

    public Photo() {
    }

    public Photo(int id, String path) {
        this.id = id;
        this.isCoverPhoto = false;
        this.isUrl = false;
        this.path = path;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isCoverPhoto() {
        return isCoverPhoto;
    }

    public void setCoverPhoto(boolean coverPhoto) {
        isCoverPhoto = coverPhoto;
    }

    public boolean isUrl() {
        return isUrl;
    }

    public void setUrl(boolean url) {
        isUrl = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
