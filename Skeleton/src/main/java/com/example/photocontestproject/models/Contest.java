package com.example.photocontestproject.models;

import com.example.photocontestproject.models.enums.Phase;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int id;

    @Column(name = "title")
    @NotNull(message = "Title field cannot be empty")
    private String title;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "open_access")
    private boolean openAccess;

    @Column(name = "creation_time")
    private LocalDateTime creationTime;

    @Column(name = "phase")
    private Phase phase;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_contests", joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> jury;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "contests_photos",
            joinColumns = {
                    @JoinColumn(name = "contest_id", referencedColumnName = "contest_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "photo_id", referencedColumnName = "photo_id")})
    private Photo coverPhoto;

    @OneToMany(mappedBy = "contest", fetch = FetchType.EAGER)
    @MapKeyColumn(name = "entry_id")
    private Set<Entry> entries;

    @JsonIgnore
    @OneToMany(mappedBy = "contest", fetch = FetchType.EAGER)
    @MapKeyColumn(name = "invite_id")
    private Set<Invite> invites;

    @Column(name = "phase_one_expiration")
    private LocalDateTime phaseOneExpiration;

    @Column(name = "phase_two_expiration")
    private LocalDateTime phaseTwoExpiration;


    public Contest() {
    }

    public Contest(int id, String title, Category category, boolean openAccess, LocalDateTime creationTime,
                   Phase phase, Photo coverPhoto, LocalDateTime phaseOneExpiration, LocalDateTime phaseTwoExpiration) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.openAccess = openAccess;
        this.creationTime = creationTime;
        this.phase = Phase.FIRST;
        this.coverPhoto = coverPhoto;
        this.phaseOneExpiration = phaseOneExpiration;
        this.phaseTwoExpiration = phaseTwoExpiration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isOpenAccess() {
        return openAccess;
    }

    public void setOpenAccess(boolean openAccess) {
        this.openAccess = openAccess;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public Phase getPhase() {
        return phase;
    }

    public void setPhase(Phase phase) {
        this.phase = phase;
    }

    public Set<User> getJury() {
        return jury;
    }

    public void setJury(Set<User> jury) {
        this.jury = jury;
    }

    public Photo getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(Photo coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public Set<Entry> getEntries() {
        return entries;
    }

    public void setEntries(Set<Entry> entries) {
        this.entries = entries;
    }

    public Set<Invite> getInvites() {
        return invites;
    }

    public void setInvites(Set<Invite> invites) {
        this.invites = invites;
    }

    public LocalDateTime getPhaseOneExpiration() {
        return phaseOneExpiration;
    }

    public void setPhaseOneExpiration(LocalDateTime phaseOneExpiration) {
        this.phaseOneExpiration = phaseOneExpiration;
    }

    public LocalDateTime getPhaseTwoExpiration() {
        return phaseTwoExpiration;
    }

    public void setPhaseTwoExpiration(LocalDateTime phaseTwoExpiration) {
        this.phaseTwoExpiration = phaseTwoExpiration;
    }

    public long calculatePhaseTimeDiff() {
        LocalDateTime timeNow = LocalDateTime.now();
        LocalDateTime timeFuture;
        long timeDifference;

        if (this.getPhase().equals(Phase.FIRST)) {
            timeFuture = this.getPhaseOneExpiration();
        } else {
            timeFuture = this.getPhaseTwoExpiration();
        }
        timeDifference = ChronoUnit.SECONDS.between(timeNow, timeFuture);
        return timeDifference;
    }

    public String getContestPhase() {
        if (LocalDateTime.now().isBefore(phaseOneExpiration)) return String.valueOf(Phase.FIRST);
        if (LocalDateTime.now().isBefore(phaseTwoExpiration)) return String.valueOf(Phase.SECOND);
        return String.valueOf(Phase.FINISHED);
    }
}
