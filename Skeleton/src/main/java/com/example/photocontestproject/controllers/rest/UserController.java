package com.example.photocontestproject.controllers.rest;

import com.example.photocontestproject.controllers.AuthenticationHelper;
import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.dtos.UpdateUserDto;
import com.example.photocontestproject.models.dtos.UserDto;
import com.example.photocontestproject.models.enums.Ranking;
import com.example.photocontestproject.models.enums.UserSortOptions;
import com.example.photocontestproject.services.contracts.UserService;
import com.example.photocontestproject.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    @Autowired
    public UserController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @GetMapping()
    public List<User> getAll(@RequestHeader HttpHeaders headers, @RequestParam(required = false) Optional<String> search) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        return userService.search(search, loggedUser);
    }

    @GetMapping("/{id}")
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        try {
            return userService.getById(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("filter")
    public List<User> filter(@RequestHeader HttpHeaders headers, @RequestParam(required = false) Optional<String> name,
                             @RequestParam(required = false) Optional<Ranking> ranking,
                             @RequestParam(required = false) Optional<UserSortOptions> sort) {
        authenticationHelper.tryGetUser(headers);
        try {
            return userService.filter(name, ranking,
                    sort.map(userSortOptions -> UserSortOptions.valueOf(String.valueOf(userSortOptions))));
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping
    public User createUser(@Valid @RequestBody UserDto userDto) {
        User user = userMapper.userFromDto(userDto);
        try {
            userService.create(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("{id}")
    public User updateUser(@RequestHeader HttpHeaders headers,
                           @PathVariable int id, @Valid @RequestBody UpdateUserDto updateUserDto) {
        User user = authenticationHelper.tryGetUser(headers);
        User userToUpdate = userMapper.userFromDto(updateUserDto, id);
        userService.update(userToUpdate, user);
        return userToUpdate;
    }

    @DeleteMapping("{id}")
    public void deleteUser(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            userService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
