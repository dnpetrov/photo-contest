package com.example.photocontestproject.services;


import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Invite;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.repositories.contracts.InviteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.photocontestproject.Helpers.*;
import static com.example.photocontestproject.Helpers.createMockUser;
import static com.example.photocontestproject.utils.constants.GlobalConstants.ORGANIZER_ROLE;

@ExtendWith(MockitoExtension.class)
public class InviteServiceTest {

    @Mock
    InviteRepository mockRepository;

    @InjectMocks
    InviteServiceImpl inviteService;

    @Test
    public void getAll_Should_ReturnAllInvites_When_InviteExist() {
        //Arrange
        Mockito.when(mockRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        inviteService.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_Should_Throw_When_InviteDoesNotExist() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenThrow(EntityNotFoundException.class);

        //Act
        inviteService.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnInvite_When_InviteExist() {
        //Arrange
        Invite mockInvite = createMockInvite();
        Mockito.when(mockRepository.getById(mockInvite.getId())).thenReturn(null);

        //Act
        inviteService.getById(mockInvite.getId());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getById(mockInvite.getId());
    }

    @Test
    public void getById_Should_Throw_When_InviteDoesNotExist() {
        //Arrange
        Invite mockInvite = createMockInvite();
        Mockito.when(mockRepository.getById(mockInvite.getId()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> inviteService.getById(mockInvite.getId()));
    }

    @Test
    public void createInvite_Should_Not_CallRepository_WhenUserAlreadyRecieveInvitation() {
        //Arrange
        Invite mockInvite = createMockInvite();

        //Act
        inviteService.create(mockInvite);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(0)).create(mockInvite);
    }

    @Test
    public void createInvite_Should_CallRepository() {
        //Arrange
        Invite mockInvite = createMockInvite();
        User mockUser = createMockUserTest();
        mockInvite.setReceiver(mockUser);
        Contest mockContest = createMockInviteContest();
        mockInvite.setContest(mockContest);
        Mockito.when(mockRepository.getByContestAndReceiver(mockContest, mockUser)).thenThrow(new EntityNotFoundException());

        //Act
        inviteService.create(mockInvite);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getByContestAndReceiver(mockContest, mockUser);
        Mockito.verify(mockRepository, Mockito.times(1)).create(mockInvite);
    }

    @Test
    public void createMassInvite_Should_Not_CallRepository_WhenUserAlreadyRecieveInvitation() {
        //Arrange
        List<Invite> invites = new ArrayList<>();
        Invite mockInvite = createMockInvite();
        invites.add(mockInvite);

        //Act
        inviteService.massCreate(invites);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(0)).create(mockInvite);
    }

    @Test
    public void createMassInvite_Should_CallRepository() {
        //Arrange
        List<Invite> invites = new ArrayList<>();
        Invite mockInvite = createMockInvite();
        User mockUser = createMockUserTest();
        mockInvite.setReceiver(mockUser);
        Contest mockContest = createMockInviteContest();
        mockInvite.setContest(mockContest);
        invites.add(mockInvite);
        Mockito.when(mockRepository.getByContestAndReceiver(mockContest, mockUser)).thenThrow(new EntityNotFoundException());

        //Act
        inviteService.massCreate(invites);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getByContestAndReceiver(mockContest, mockUser);
        Mockito.verify(mockRepository, Mockito.times(1)).create(mockInvite);
    }

    @Test
    public void deleteInvite_Should_CallRepository() {
        //Arrange
        Invite mockInvite = createMockInvite();

        //Act
        inviteService.delete(mockInvite);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).delete(mockInvite.getId());
    }

    @Test
    public void isUserInvitedOrOrganizer_should_returnFalse_WhenInputInvalid() {
        //Arrange
        Invite mockInvite = createMockInvite();
        User mockUser = createMockUser();
        mockInvite.setReceiver(mockUser);
        Contest mockContest = createMockInviteContest();
        mockInvite.setContest(mockContest);
        Mockito.when(mockRepository.getByContestAndReceiver(mockContest, mockUser)).thenThrow(EntityNotFoundException.class);

        // Act
        inviteService.isUserInvitedOrOrganizer(mockUser,mockContest);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getByContestAndReceiver(mockContest, mockUser);
        Assertions.assertFalse(inviteService.isUserInvitedOrOrganizer(mockUser, mockContest));
    }

    @Test
    public void isUserInvitedOrOrganizer_should_returnTrue_whenInputValid() {
        //Arrange
        Invite mockInvite = createMockInvite();
        User mockUser = createMockOrganizer();
        mockInvite.setReceiver(mockUser);
        Contest mockContest = createMockInviteContest();
        mockInvite.setContest(mockContest);
        Mockito.when(mockRepository.getByContestAndReceiver(mockContest, mockUser)).thenReturn(mockInvite);

        // Act
        inviteService.isUserInvitedOrOrganizer(mockUser,mockContest);
        boolean isInvited = mockUser.getRole().getRole().equals(ORGANIZER_ROLE);

        //Assert
        Assertions.assertEquals(isInvited,inviteService.isUserInvitedOrOrganizer(mockUser,mockContest));
        Mockito.verify(mockRepository, Mockito.times(2))
                .getByContestAndReceiver(mockContest, mockUser);
    }
}
