package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Invite;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Ranking;
import com.example.photocontestproject.models.enums.UserSortOptions;
import com.example.photocontestproject.repositories.contracts.InviteRepository;
import com.example.photocontestproject.repositories.contracts.RoleRepository;
import com.example.photocontestproject.repositories.contracts.UserRepository;
import com.example.photocontestproject.services.contracts.EntryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.photocontestproject.Helpers.*;
import static com.example.photocontestproject.utils.constants.GlobalConstants.ORGANIZER_ROLE;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    UserRepository userMockRepository;
    @Mock
    RoleRepository roleMockRepository;
    @Mock
    InviteRepository inviteMockRepository;
    @Mock
    EntryService entryMockService;

    @InjectMocks
    UserServiceImpl userMockService;

    @Test
    public void getAll_should_returnEmptyList_whenNoUsers() {
        //Arrange
        User mockUser = createMockOrganizer();
        Mockito.when(userMockRepository.getAll())
                .thenThrow(EntityNotFoundException.class);

        // Act
        //Assert
        Assertions.assertEquals(0,userMockService.getAll(mockUser).size());
    }

    @Test
    public void getAll_should_callRepository() {
        //Arrange
        User mockUser = createMockOrganizer();
        Mockito.when(userMockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        userMockService.getAll(mockUser);

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAll_should_throw_whenUserNotOrganizer() {
        //Arrange
        User mockUser = createMockUser();

        // Act
        //Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,() -> userMockService.getAll(mockUser));
    }

    @Test
    public void getCountRegularUsers_should_callRepository() {
        //Arrange
        Mockito.when(userMockRepository.getCountAllRegularUsers())
                .thenReturn(1);

        // Act
        userMockService.getCountAllRegularUsers();

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1))
                .getCountAllRegularUsers();
        Assertions.assertEquals(1, userMockService.getCountAllRegularUsers());
    }

    @Test
    public void getById_should_returnUser_when_matchExist() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userMockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        // Act
        User user = userMockService.getById(mockUser.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), user.getId()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), user.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), user.getLastName()),
                () -> Assertions.assertEquals(mockUser.getEmail(), user.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUsername(), user.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(), user.getPassword()),
                () -> Assertions.assertEquals(mockUser.getRole(), user.getRole()),
                () -> Assertions.assertEquals(mockUser.getPoints(), user.getPoints()),
                () -> Assertions.assertEquals(mockUser.getRanking(), user.getRanking())        );
    }

    @Test
    public void getById_should_returnUser_when_matchExistOverloaded() {
        //Arrange
        User mockUser = createMockUser();
        User mockOrganizer = createMockOrganizer();
        Mockito.when(userMockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        // Act
        User user = userMockService.getById(mockUser.getId(),mockOrganizer);

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), user.getId()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), user.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), user.getLastName()),
                () -> Assertions.assertEquals(mockUser.getEmail(), user.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUsername(), user.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(), user.getPassword()),
                () -> Assertions.assertEquals(mockUser.getRole(), user.getRole()),
                () -> Assertions.assertEquals(mockUser.getPoints(), user.getPoints()),
                () -> Assertions.assertEquals(mockUser.getRanking(), user.getRanking())        );
    }

    @Test
    public void getById_should_throw_whenUserNotOrganizer() {
        //Arrange
        User mockUser = createMockUser();
        User mockLoggedUser = createMockUser();
        mockUser.setId(2);

        // Act
        //Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userMockService.getById(mockUser.getId(),mockLoggedUser));
    }

    @Test
    public void getById_should_throw_whenNoUsersFound() {
        //Arrange
        User mockUser = createMockUser();
        User mockLoggedUser = createMockOrganizer();
        Mockito.when(userMockRepository.getById(mockUser.getId()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userMockService.getById(mockUser.getId(),mockLoggedUser));
    }

    @Test
    public void getById_should_throw_whenNoUsersFoundOverloaded() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userMockRepository.getById(mockUser.getId()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userMockService.getById(mockUser.getId()));
    }

    @Test
    public void getByUsername_should_returnUser_when_matchExist() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userMockRepository.getByField("username", mockUser.getUsername()))
                .thenReturn(mockUser);

        // Act
        User user = userMockService.getByUsername(mockUser.getUsername());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), user.getId()),
                () -> Assertions.assertEquals(mockUser.getFirstName(), user.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), user.getLastName()),
                () -> Assertions.assertEquals(mockUser.getEmail(), user.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUsername(), user.getUsername()),
                () -> Assertions.assertEquals(mockUser.getPassword(), user.getPassword()),
                () -> Assertions.assertEquals(mockUser.getRole(), user.getRole()),
                () -> Assertions.assertEquals(mockUser.getPoints(), user.getPoints()),
                () -> Assertions.assertEquals(mockUser.getRanking(), user.getRanking())        );
    }

    @Test
    public void getByUsername_should_throw_whenNoUsersFoundOverloaded() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userMockRepository.getByField("username", mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userMockService.getByUsername(mockUser.getUsername()));
    }

    @Test
    public void getAllOrganizers_should_callRepository() {
        //Arrange
        Mockito.when(userMockRepository.getAllOrganizers())
                .thenReturn(new ArrayList<>());

        // Act
        userMockService.getAllOrganizers();

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1))
                .getAllOrganizers();
    }

    @Test
    public void getAllOrganizers_should_returnEmptyList_whenNoUsersFound() {
        //Arrange
        Mockito.when(userMockRepository.getAllOrganizers())
                .thenThrow(EntityNotFoundException.class);

        // Act
        //Assert
        Assertions.assertEquals(0,userMockService.getAllOrganizers().size());
    }

    @Test
    public void getAvailableUsersForInvite_should_returnBlankList_whenNoAvailableUsers() {
        //Arrange
        Mockito.when(userMockRepository.getAvailableUsersForInvite(1))
                .thenThrow(EntityNotFoundException.class);
        // Act
        List<User> users = userMockService.getAvailableUsersForInvite(1);

        //Assert
        Assertions.assertEquals(0,users.size());
    }

    @Test
    public void getAvailableUsersForInvite_should_calLRepository_whenInputValid() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(userMockRepository.getAvailableUsersForInvite(1))
                .thenReturn(new ArrayList<>());
        // Act
        userMockService.getAvailableUsersForInvite(1);

        //Assert
        Mockito.verify(userMockRepository,
                Mockito.times(1)).getAvailableUsersForInvite(mockUser.getId());
    }

    @Test
    public void getAllEligibleForJury_should_returnBlankList_whenNoAvailableUsers() {
        //Arrange
        Mockito.when(userMockRepository.getAllEligibleForJury())
                .thenThrow(EntityNotFoundException.class);
        // Act
        List<User> users = userMockService.getAllEligibleForJury();

        //Assert
        Assertions.assertEquals(0,users.size());
    }

    @Test
    public void getAllEligibleForJury_should_calLRepository_whenInputValid() {
        //Arrange
        Mockito.when(userMockRepository.getAllEligibleForJury())
                .thenReturn(new ArrayList<>());
        // Act
        userMockService.getAllEligibleForJury();

        //Assert
        Mockito.verify(userMockRepository,
                Mockito.times(1)).getAllEligibleForJury();
    }

    @Test
    public void getContestJury_should_returnBlankList_whenNoAvailableUsers() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        Mockito.when(userMockRepository.getContestJury(mockContest))
                .thenThrow(EntityNotFoundException.class);

        // Act
        //Assert
        Assertions.assertEquals(0,userMockService.getContestJury(mockContest).size());
    }

    @Test
    public void getContestJury_should_callRepository() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        Mockito.when(userMockRepository.getContestJury(mockContest))
                .thenReturn(new ArrayList<>());

        // Act
        userMockService.getContestJury(mockContest);

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1))
                .getContestJury(mockContest);
    }

    @Test
    public void isUserJury_should_callRepository_whenInputValid() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        User mockUser = createMockUser();
        List<User> userList = new ArrayList<>();
        userList.add(mockUser);
        Mockito.when(userMockRepository.getContestJury(mockContest))
                .thenReturn(userList);

        // Act
        userMockService.isUserJury(mockUser,mockContest);

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1))
                .getContestJury(mockContest);
    }

    @Test
    public void isUserJury_should_returnTrue_whenInputValid() {
        //Arrange
        Contest mockContest = createMockOpenContest();
        User mockUser = createMockUser();
        List<User> userList = new ArrayList<>();
        userList.add(mockUser);
        Mockito.when(userMockRepository.getContestJury(mockContest))
                .thenReturn(userList);

        // Act
        List<User> jury = userMockService.getContestJury(mockContest);
        boolean isJury = jury.contains(mockUser);

        //Assert
        Assertions.assertEquals(isJury,userMockService.isUserJury(mockUser,mockContest));
    }

    @Test
    public void search_should_throw_whenUserNotOrganizer() {
        //Arrange
        User mockUser = createMockUser();
        Optional<String> search = Optional.of("mockString");

        // Act
        //Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,() -> userMockService.search(search,mockUser));
    }

    @Test
    public void search_should_call_repository_whenSearchNotEmpty() {
        // Arrange
        User mockUser = createMockOrganizer();
        Optional<String> search = Optional.of("mockSearch");
        Mockito.when(userMockRepository.search(search))
                .thenReturn(new ArrayList<>());

        // Act
        userMockService.search(search,mockUser);

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).search(search);
    }

    @Test
    public void search_should_call_repository_whenSearchEmpty() {
        // Arrange
        User mockUser = createMockOrganizer();
        Optional<String> search = Optional.empty();
        Mockito.when(userMockRepository.search(search))
                .thenReturn(new ArrayList<>());

        // Act
        userMockService.search(search,mockUser);

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).search(search);
    }

    @Test
    public void search_should_call_repository_whenSearchNull() {
        // Arrange
        User mockUser = createMockOrganizer();
        Optional<String> search = Optional.ofNullable(null);
        Mockito.when(userMockRepository.search(search))
                .thenReturn(new ArrayList<>());

        // Act
        userMockService.search(search,mockUser);

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).search(search);
    }

    @Test
    public void filter_should_call_repository_whenSearchNotEmpty() {
        // Arrange
        Optional<String> name = Optional.of("mockName");
        Optional<Ranking> ranking = Optional.of(Ranking.JUNKIE);
        Optional<UserSortOptions> sort = Optional.of(UserSortOptions.NAME_ASC);
        Mockito.when(userMockRepository.filter(name,ranking,sort))
                .thenReturn(new ArrayList<>());

        // Act
        userMockService.filter(name,ranking,sort);

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).filter(name,ranking,sort);
    }

    @Test
    public void filter_should_call_repository_whenSearchEmpty() {
        // Arrange
        Optional<String> name = Optional.empty();
        Optional<Ranking> ranking = Optional.of(Ranking.JUNKIE);
        Optional<UserSortOptions> sort = Optional.of(UserSortOptions.NAME_ASC);
        Mockito.when(userMockRepository.filter(name,ranking,sort))
                .thenReturn(new ArrayList<>());

        // Act
        userMockService.filter(name,ranking,sort);

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).filter(name,ranking,sort);
    }

    @Test
    public void filter_should_call_repository_whenSearchNull() {
        // Arrange
        Optional<String> name = Optional.ofNullable(null);
        Optional<Ranking> ranking = Optional.of(Ranking.JUNKIE);
        Optional<UserSortOptions> sort = Optional.of(UserSortOptions.NAME_ASC);
        Mockito.when(userMockRepository.filter(name,ranking,sort))
                .thenReturn(new ArrayList<>());

        // Act
        userMockService.filter(name,ranking,sort);

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).filter(name,ranking,sort);
    }

    @Test
    public void create_should_throw_when_userWithSameUsernameExists() {
        // Arrange
        User mockUser = createMockUser();
        User userInputMockUser = createMockUser();
        userInputMockUser.setId(mockUser.getId() + 1);
        Mockito.when(userMockRepository.getByField("Username",mockUser.getUsername()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> userMockService.create(userInputMockUser));
    }

    @Test
    public void create_should_throw_when_userWithSameEmailExists() {
        // Arrange
        User mockUser = createMockUser();
        User userInputMockUser = createMockUser();
        userInputMockUser.setId(mockUser.getId() + 1);
        Mockito.when(userMockRepository.getByField("Username",mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(userMockRepository.getByField("Email",mockUser.getEmail()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> userMockService.create(userInputMockUser));
    }

    @Test
    public void create_should_callRepository_when_userWithSameUsernameDoesNotExist() {
        // Arrange
        User mockUser = createMockUser();
        Mockito.when(userMockRepository.getByField("Username",mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(userMockRepository.getByField("Email",mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);
        // Act
        userMockService.create(mockUser);

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).create(mockUser);
    }

    @Test
    public void update_should_throw_whenUserNotOrganizer() {
        //Arrange
        User mockUser = createMockUser();
        User mockUserToUpdate = createMockUser();
        mockUserToUpdate.setId(2);

        // Act
        //Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,() -> userMockService.update(mockUserToUpdate,mockUser));
    }

    @Test
    public void update_should_callRepository_when_userWithSameId() {
        // Arrange
        User mockUser = createMockUser();
        User mockUserToUpdate = createMockUser();

        // Act
        userMockService.update(mockUserToUpdate,mockUser);

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).update(mockUserToUpdate);
    }

    @Test
    public void promote_should_throw_whenUserNotOrganizer() {
        //Arrange
        User mockUser = createMockUser();
        User mockUserToUpdate = createMockUser();
        mockUserToUpdate.setId(2);

        // Act
        //Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,() -> userMockService.promote(mockUser,mockUserToUpdate));
    }

    @Test
    public void promote_should_setRoleToOrganizer_whenInputValid() {
        //Arrange
        User mockOrganizer = createMockOrganizer();
        User mockUserToUpdate = createMockUser();
        User toCheckAgainst = createMockOrganizer();
        Mockito.when(roleMockRepository.getByRole(ORGANIZER_ROLE))
                .thenReturn(createMockRoleOrganizer());

        // Act
        User updatedUser = userMockService.promote(mockOrganizer,mockUserToUpdate);
        //Assert
        Assertions.assertEquals(updatedUser.getRole().getRole(),toCheckAgainst.getRole().getRole());
    }

    @Test
    public void promote_should_callRepository_whenInputValid() {
        //Arrange
        User mockOrganizer = createMockOrganizer();
        User mockUserToUpdate = createMockUser();

        // Act
        userMockService.promote(mockOrganizer,mockUserToUpdate);

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).update(mockUserToUpdate);

    }

    @Test
    public void delete_should_throw_whenUserNotOrganizer() {
        //Arrange
        User mockUser = createMockUser();
        User mockUserToUpdate = createMockUser();

        // Act
        //Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userMockService.delete(mockUserToUpdate.getId(),mockUser));
    }

    @Test
    public void delete_should_throw_whenUserToDeleteIsOrganizer() {
        //Arrange
        User mockUser = createMockOrganizer();
        User mockUserToUpdate = createMockOrganizer();
        Mockito.when(userMockRepository.getById(mockUserToUpdate.getId())).thenReturn(mockUserToUpdate);

        // Act
        //Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userMockService.delete(mockUserToUpdate.getId(),mockUser));
    }

    @Test
    public void delete_should_callInviteRepository_whenThereAreInvites() {
        //Arrange
        User mockOrganizer = createMockOrganizer();
        User mockUserToDelete = createMockUser();
        Invite mockInvite = createMockInvite();
        List<Invite> invites = new ArrayList<>();
        invites.add(mockInvite);
        Mockito.when(userMockRepository.getById(mockUserToDelete.getId()))
                .thenReturn(mockUserToDelete);
        Mockito.when(userMockRepository.getAllInvites(mockUserToDelete))
                .thenReturn(invites);

        // Act
        userMockService.delete(mockUserToDelete.getId(),mockOrganizer);

        //Assert
        Mockito.verify(inviteMockRepository, Mockito.times(1)).delete(mockInvite.getId());
    }

    @Test
    public void delete_should_callEntryRepository_whenThereAreEntries() {
        //Arrange
        User mockOrganizer = createMockOrganizer();
        User mockUserToDelete = createMockUser();
        Entry mockEntry = createMockEntry();
        List<Entry> entries = new ArrayList<>();
        entries.add(mockEntry);
        Mockito.when(userMockRepository.getById(mockUserToDelete.getId()))
                .thenReturn(mockUserToDelete);
        Mockito.when(userMockRepository.getAllEntries(mockUserToDelete))
                .thenReturn(entries);

        // Act
        userMockService.delete(mockUserToDelete.getId(),mockOrganizer);

        //Assert
        Mockito.verify(entryMockService, Mockito.times(1)).delete(mockEntry.getId(),mockOrganizer);
    }

    @Test
    public void delete_should_callUserRepository_whenInputValid() {
        //Arrange
        User mockOrganizer = createMockOrganizer();
        User mockUserToDelete = createMockUser();
        Mockito.when(userMockRepository.getById(mockUserToDelete.getId()))
                .thenReturn(mockUserToDelete);

        // Act
        userMockService.delete(mockUserToDelete.getId(),mockOrganizer);

        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).delete(mockUserToDelete.getId());
    }

}
