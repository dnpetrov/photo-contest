package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Review;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.repositories.contracts.EntryRepository;
import com.example.photocontestproject.repositories.contracts.ReviewRepository;
import com.example.photocontestproject.repositories.contracts.UserRepository;
import com.example.photocontestproject.services.contracts.EntryService;
import com.example.photocontestproject.services.contracts.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.example.photocontestproject.utils.constants.GlobalConstants.*;

@Service
public class EntryServiceImpl implements EntryService {

    private final EntryRepository entryRepository;
    private final ReviewRepository reviewRepository;
    private final UserRepository userRepository;
    private final ReviewService reviewService;

    @Autowired
    public EntryServiceImpl(EntryRepository entryRepository, ReviewRepository reviewRepository, UserRepository userRepository, ReviewService reviewService) {
        this.entryRepository = entryRepository;
        this.reviewRepository = reviewRepository;
        this.userRepository = userRepository;
        this.reviewService = reviewService;
    }

    @Override
    public Entry getById(int id) {
        try {
            return entryRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public Entry getByTitle(String title) {
        try {
            return entryRepository.getByField(TITLE_LOWERCASE, title);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public Entry getByUserAndContestId(User user, int contestId) {
        try {
            return entryRepository.getByUserAndContestId(user, contestId);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public List<Entry> getAllByContestId(int id) {
        try {
            return entryRepository.getAllByContestId(id);
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Entry> getAllByUser(User user) {
        try {
            return entryRepository.getAllByUser(user);
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Entry> getAllForOpenContests() {
        try {
            return entryRepository.getAllForOpenContests();
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Entry> getAll() {
        try {
            return entryRepository.getAll();
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public int getCountAllEntries() {
        return entryRepository.getCountAllEntries();
    }

    @Override
    public int getCountByContestId(int id) {
        try {
            return entryRepository.getCountByContestId(id);
        } catch (EntityNotFoundException e) {
            return 0;
        }
    }

    @Override
    public void create(Entry entry, User loggedUser) {
        try {
            getByUserAndContestId(loggedUser, entry.getContest().getId());
        } catch (EntityNotFoundException e) {
            entryRepository.create(entry);
        }
    }

    @Override
    public void update(Entry entry, User loggedUser) {
        if (!loggedUser.getRole().getRole().equals(ORGANIZER_ROLE)) {
            throw new UnauthorizedOperationException(ONLY_USERS_WITH_ORGANIZER_ROLE_COULD_UPDATE_ENTRY);
        }
        entryRepository.update(entry);
    }

    @Override
    public void delete(int id, User loggedUser) {
        Entry entry = getById(id);
        if (!userRepository.getContestJury(entry.getContest()).contains(loggedUser)) {
            throw new UnauthorizedOperationException(ONLY_USERS_WITH_ORGANIZER_ROLE_COULD_DELETE_ENTRY);
        }

        List<Review> reviews = entryRepository.getEntryReviews(entry);
        if (!reviews.isEmpty()) {
            for (Review review : reviews) {
                reviewRepository.delete(review.getId());
            }
        }
        entryRepository.delete(id);
    }

    @Override
    public HashMap<Entry, Double> getFinalScoresForEntriesList(List<Entry> entries) {
        HashMap<Entry, Double> scores = new HashMap<>();
        for (Entry ent : entries) {
            double finalScore = reviewService.getEntryFinalScore(ent);
            finalScore = Math.round(finalScore * 100.0) / 100.0;
            scores.put(ent, finalScore);
        }
        return scores;
    }

    @Override
    public Map<Integer, List<Entry>> getWinnerOrWinners(Contest contest) {

        Map<Integer, List<Entry>> winners = new HashMap<>();
        List<Entry> allEntries;
        try {
            allEntries = entryRepository.getAllByContestId(contest.getId());
        } catch (EntityNotFoundException e) {
            return winners;
        }

        int position = 1;

        while (allEntries.size() > 0) {
            Entry entry = getMaxScoreEntryFromListOfEntries(allEntries);
            if (!winners.isEmpty()) {
                double previousScore = reviewRepository.avgEntryScore(winners.get(position).get(0));
                double currentScore = reviewRepository.avgEntryScore(entry);
                if (previousScore != currentScore) position++;
            }
            if (position > 3) break;
            List<Entry> tempList;
            if (winners.containsKey(position)) {
                tempList = winners.get(position);
                tempList.add(entry);
                winners.put(position, tempList);
                allEntries.remove(entry);
            } else {
                tempList = new ArrayList<>();
                tempList.add(entry);
                winners.put(position, tempList);
                allEntries.remove(entry);
            }
        }
        if (allEntries.size() > 0) {
            addScoresToRestOfParticipants(allEntries);
        }
        return winners;
    }

    @Override
    public Map<Contest, Map<Integer, List<Entry>>> getAllWinners(List<Contest> finished) {
        Map<Contest, Map<Integer, List<Entry>>> winners = new HashMap<>();
        Map<Integer, List<Entry>> inner;

        for (Contest contest : finished) {
            inner = getWinnerOrWinners(contest);
            winners.put(contest, inner);
        }
        return winners;
    }

    @Override
    public void calculateWinners(Contest contest) {
        Map<Integer, List<Entry>> winners = getWinnerOrWinners(contest);

        boolean scoreIsDouble = false;
        if (winners.containsKey(1) && winners.containsKey(2)) {
            double firstPlaceScore = reviewRepository.avgEntryScore(winners.get(1).get(0));
            double secondPlaceScore = reviewRepository.avgEntryScore(winners.get(2).get(0));
            if (firstPlaceScore / secondPlaceScore >= 2) scoreIsDouble = true;
        }

        if (winners.containsKey(1)) addScoresToWinners(winners.get(1), 1, scoreIsDouble);
        if (winners.containsKey(2)) addScoresToWinners(winners.get(2), 2, false);
        if (winners.containsKey(3)) addScoresToWinners(winners.get(3), 3, false);
    }

    @Override
    public List<Entry> search(Optional<String> search, User loggedUser) {
        if (!loggedUser.getRole().getRole().equals(ORGANIZER_ROLE))
            throw new UnauthorizedOperationException(INFORMATION_ERROR_MESSAGE);
        return entryRepository.search(search);
    }

    private void addScoresToWinners(List<Entry> entries, int position, boolean scoreIsDouble) {
        for (Entry entry : entries) {
            int score = 0;
            if (entries.get(0).isInvited()) {
                score += 3;
            } else {
                score++;
            }
            if (position == 1) score += 50;
            else if (position == 2) score += 35;
            else score += 20;
            if (scoreIsDouble) score += 25;
            User user = userRepository.getById(entry.getParticipant().getId());
            user.setPoints(user.getPoints() + score);
            user.setRanking(user.getRanking());
            userRepository.update(user);
        }
    }

    private void addScoresToRestOfParticipants(List<Entry> entries) {
        for (Entry entry : entries) {
            int score = 0;
            if (entries.get(0).isInvited()) {
                score += 3;
            } else {
                score++;
            }
            User user = userRepository.getById(entry.getParticipant().getId());
            user.setPoints(user.getPoints() + score);
            user.setRanking(user.getRanking());
            userRepository.update(user);
        }
    }

    private Entry getMaxScoreEntryFromListOfEntries(List<Entry> entries) {
        return entries
                .stream()
                .max(Comparator.comparing(reviewRepository::avgEntryScore))
                .orElse(new Entry());
    }
}
