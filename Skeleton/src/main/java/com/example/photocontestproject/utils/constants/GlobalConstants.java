package com.example.photocontestproject.utils.constants;

public class GlobalConstants {

    public static final String ORGANIZER_ROLE = "Organizer";
    public static final String USER_ROLE = "User";

    public static final String UPLOADED_FOLDER_COVER_PHOTO =
            "D://Telerik//Repos//photo-contest//Skeleton//src//main//resources//static//images//upload//cover//";
    public static final String UPLOADED_FOLDER_ENTRY_PHOTO =
            "D://Telerik//Repos//photo-contest//Skeleton//src//main//resources//static//images//upload//";

    public static final String READ_FOLDER_COVER = "/images/upload/cover/";
    public static final String UPLOAD_FOLDER = "/images/upload/";
    public static final String PLEASE_SELECT_A_FILE_TO_UPLOAD = "Please select a file to upload";

    public static final String CONTEST_UPPERCASE = "Contest";
    public static final String USER_LOWERCASE = "user";
    public static final String PHASE_LOWERCASE = "phase";
    public static final String ENTRY_UPPERCASE = "Entry";
    public static final String CONTEST_ID = "contest ID";
    public static final String TITLE_LOWERCASE = "title";

    public static final String CHANGED_PHASE_TWO = " changed to phase two";
    public static final String CHANGED_FINISHED = " time expired and is now in finished phase";
    public static final String CONTEST_CHECKED = "All not finished contests checked at :";
    public static final String ONLY_USERS_WITH_ORGANIZER_ROLE_COULD_CREATE_CONTESTS = "Only users with Organizer role could create contests";
    public static final String ONLY_USERS_WITH_ORGANIZER_ROLE_COULD_DELETE_ENTRY= "Only users with Organizer role could delete entry";
    public static final String ONLY_USERS_WITH_ORGANIZER_ROLE_COULD_UPDATE_ENTRY= "Only users with Organizer role could update entry";

    public static final int PHOTO_ENTHUSIAST_MIN_SCORE = 50;
    public static final int PHOTO_MASTER_MIN_SCORE = 150;
    public static final int PHOTO_DICTATOR_MIN_SCORE = 1000;

    public static final String ORGANIZER_PROMOTE_ERROR_MESSAGE = "Only Organizers can promote users to organizers.";
    public static final String ORGANIZER_SEARCH_ERROR_MESSAGE = "Only Organizers can search users.";
    public static final String USER_EDIT_ERROR_MESSAGE = "A user could change only own profile details.";
    public static final String ORGANIZER_DELETE_ERROR_MESSAGE = "Only Organizers can delete users.";
    public static final String INFORMATION_ERROR_MESSAGE = "This information can only be viewed by Organizers.";
    public static final String USER_VIEW_ERROR_MESSAGE = "User information can only be viewed by Organizers or the user themselves.";
    public static final String ORGANIZER_NOT_TO_BE_DELETED_ERROR_MESSAGE = "Organizers can not be deleted.";

    public static final String SECOND_PHASE = "Second";
    public static final String SECOND_PHASE_ERROR_MESSAGE = "Reviews can only be submitted when a contest is in second phase.";
    public static final String FINISHED_PHASE = "Finished";
    public static final String FINISHED_PHASE_ERROR_MESSAGE = "Final score is available only in finished phase";
    public static final String DEFAULT_COMMENT = "Photo is not reviewed, a default score of 3 is awarded.";
    public static final int DEFAULT_SCORE = 3;
    public static final String JUROR_ALREADY_REVIEWED_ERROR_MESSAGE = "Entry is already reviewed by this juror";

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";
    public static final String USER_LOGGED_IN_ERROR_MESSAGE = "No user logged in.";
    public static final String CURRENT_USER = "currentUser";
    public static final String INVALID_USERNAME_ERROR_MESSAGE = "Invalid username";
    public static final String UNAUTHORIZED_ERROR_MESSAGE = "The requested resources requires authentication";
}
