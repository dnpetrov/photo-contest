package com.example.photocontestproject.repositories;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Review;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Phase;
import com.example.photocontestproject.repositories.contracts.EntryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.example.photocontestproject.utils.constants.GlobalConstants.*;

@Repository
public class EntryRepositoryImpl extends AbstractCRUDRepository<Entry> implements EntryRepository {


    private final SessionFactory sessionFactory;

    @Autowired
    public EntryRepositoryImpl(SessionFactory sessionFactory) {
        super(Entry.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Entry> getAllByContestId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery("select distinct e from Entry e " + "join e.contest  c where c.id = :id", Entry.class);
            query.setParameter("id", id);

            List<Entry> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(ENTRY_UPPERCASE, CONTEST_ID, String.valueOf(id));
            }
            return result;
        }
    }

    @Override
    public int getCountByContestId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery("select count(*) from Entry e " + "join e.contest  c where c.id = :id", Long.class);
            query.setParameter("id", id);

            return Integer.parseInt(String.valueOf(query.uniqueResult()));
        }
    }

    @Override
    public int getCountAllEntries() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery("select count(*) from Entry e", Long.class);
            return Integer.parseInt(String.valueOf(query.uniqueResult()));
        }
    }

    @Override
    public List<Entry> getAllByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery("select distinct e from Entry e " + "join e.contest  c where e.participant.id = :id", Entry.class);
            query.setParameter("id", user.getId());

            List<Entry> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(ENTRY_UPPERCASE, USER_LOWERCASE, String.valueOf(user.getId()));
            }
            return result;
        }
    }

    @Override
    public Entry getByUserAndContestId(User user, int contestId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery("select distinct e from Entry e " + "join e.contest  c where c.id = :id and e.participant.id = :userId", Entry.class);
            query.setParameter("id", contestId);
            query.setParameter("userId", user.getId());

            List<Entry> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(ENTRY_UPPERCASE, CONTEST_ID, String.valueOf(contestId));
            }
            return result.get(0);
        }
    }

    @Override
    public List<Entry> getAllForOpenContests() {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery("select distinct e from Entry e "
                    + "join e.contest  c where c.phase = :phaseF or c.phase = :phaseS", Entry.class);
            query.setParameter("phaseF", Phase.FIRST);
            query.setParameter("phaseS", Phase.SECOND);

            List<Entry> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("There are no entries to open contests currently");
            }
            return result;
        }
    }

    public List<Review> getEntryReviews(Entry entry) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery("from Review where entry = :entry", Review.class);
            query.setParameter("entry", entry);
            return query.list();
        }
    }

    @Override
    public List<Entry> search(Optional<String> search) {
        if (search.isEmpty()) {
            return getAll();
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery("from Entry where title like :title", Entry.class);
            query.setParameter("title", "%" + search.get() + "%");
            return query.list();
        }
    }
}
