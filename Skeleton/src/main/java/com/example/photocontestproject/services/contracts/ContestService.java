package com.example.photocontestproject.services.contracts;

import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Phase;

import java.util.List;
import java.util.Optional;

public interface ContestService {

    List<Contest> getAll();

    Contest getById(int id);

    Contest getByTitle(String title);

    List<Contest> getOpenByUser(User user);

    List<Contest> getFinishedByUser(User user);

    List<Contest> getAllNotFinished(User loggedUser);

    List<Contest> getAllPhaseOne(int maxResults, User loggedUser);

    List<Contest> getAllPhaseTwo(int maxResults, User loggedUser);

    List<Contest> getAllPhaseFinished(int maxResults, User loggedUser);

    List<Contest> getMostRecentlyFinished();

    int getCountAllContests();

    void create(Contest contest, User loggedUser);

    void update(Contest contest, User loggedUser);

    void delete(int id, User loggedUser);

    long calculatePhaseTimeDiff(Contest contest);

    void progressPhase();

    List<Contest> filter(Optional<String> title, Optional<String> openAccess,
                         Optional<Phase> phase, Optional<Integer> categoryId);
}
