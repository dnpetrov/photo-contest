package com.example.photocontestproject.models.dtos;

import javax.validation.constraints.NotEmpty;

public class InviteDto {

    private int id;

    @NotEmpty(message = "At least one user must be selected in order to send a valid invite")
    private Integer[] receiverIds;

    public InviteDto() {
    }

    public InviteDto(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer[] getReceiverIds() {
        return receiverIds;
    }

    public void setReceiverIds(Integer[] receiverIds) {
        this.receiverIds = receiverIds;
    }
}
