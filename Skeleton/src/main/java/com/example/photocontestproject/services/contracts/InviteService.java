package com.example.photocontestproject.services.contracts;

import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Invite;
import com.example.photocontestproject.models.User;

import java.util.List;

public interface InviteService {

    List<Invite> getAll();

    Invite getById(int id);

    void create(Invite invite);

    void massCreate(List<Invite> invites);

    void delete(Invite invite);

    boolean isUserInvitedOrOrganizer(User loggedUser, Contest contest);
}
