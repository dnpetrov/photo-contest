package com.example.photocontestproject.models;

import com.example.photocontestproject.models.enums.Ranking;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

import static com.example.photocontestproject.utils.constants.GlobalConstants.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "first_name")
    @NotNull(message = "First name cannot be empty")
    private String firstName;

    @Column(name = "last_name")
    @NotNull(message = "Last name cannot be empty")
    private String lastName;

    @Column(name = "username")
    @NotNull(message = "Username cannot be empty")
    private String username;

    @Column(name = "password")
    @NotNull(message = "Password cannot be empty")
    private String password;

    @Column(name = "email")
    @NotNull(message = "Email cannot be empty")
    private String email;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;

    @Column(name = "points")
    private int points;

    @Column(name = "ranking")
    private Ranking ranking;

    @Column(name = "creation_time")
    private LocalDateTime creationTime;


    @JsonIgnore
    @OneToMany(mappedBy = "receiver", fetch = FetchType.EAGER)
    @MapKeyColumn(name = "invite_id")
    private Set<Invite> invites;

    public User() {
    }

    public User(int id, String firstName, String lastName, String username, String password, String email, Role role) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
        this.role = role;
        this.points = 0;
        this.ranking = Ranking.JUNKIE;
        this.creationTime = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setRanking(Ranking ranking) {
        this.ranking = ranking;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public Ranking getRanking() {
        if (this.getPoints() <= 50) {
            return Ranking.JUNKIE;
        } else if (this.getPoints() <= 150) {
            return Ranking.ENTHUSIAST;
        } else if (this.getPoints() <= 1000) {
            return Ranking.MASTER;
        } else {
            return Ranking.DICTATOR;
        }
    }

    public String getRankingAsString() {
        if (getPoints() <= 50) {
            return Ranking.JUNKIE.toString();
        }
        if (getPoints() <= 150) {
            return Ranking.ENTHUSIAST.toString();
        }
        if (getPoints() <= 1000) {
            return Ranking.MASTER.toString();
        }
        return Ranking.DICTATOR.toString();
    }

    public String getPointsUntilNextRanking() {
        if (getPoints() < PHOTO_ENTHUSIAST_MIN_SCORE) {
            return String.valueOf(PHOTO_ENTHUSIAST_MIN_SCORE + 1 - getPoints());
        } else if (getPoints() < PHOTO_MASTER_MIN_SCORE) {
            return String.valueOf(PHOTO_MASTER_MIN_SCORE + 1 - getPoints());
        } else if (getPoints() < PHOTO_DICTATOR_MIN_SCORE) {
            return String.valueOf(PHOTO_DICTATOR_MIN_SCORE + 1 - getPoints());
        } else {
            return String.valueOf(0);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return id == user.id && username.equals(user.username) && email.equals(user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, email);
    }
}
