package com.example.photocontestproject.utils.mappers;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Category;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.dtos.ContestDto;
import com.example.photocontestproject.models.enums.Phase;
import com.example.photocontestproject.services.contracts.CategoryService;
import com.example.photocontestproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class ContestMapper {

    private final CategoryService categoryService;
    private final UserService userService;

    @Autowired
    public ContestMapper(CategoryService categoryService, UserService userService) {
        this.categoryService = categoryService;
        this.userService = userService;
    }

    public Contest dtoToObject(ContestDto contestDto) {
        Contest contest = new Contest();
        contest.setTitle(contestDto.getTitle());
        Category category = categoryService.getByName(contestDto.getCategoryName());
        contest.setCategory(category);
        contest.setOpenAccess(contestDto.getOpenAccess().equals("Open"));

        Set<User> jury = new HashSet<>(userService.getAllOrganizers());
        if (contestDto.getAdditionalJury() != null) {
            for (int additionalJury: contestDto.getAdditionalJury()) {
                jury.add(userService.getById(additionalJury));
            }
        }
        contest.setJury(jury);
        LocalDateTime time = LocalDateTime.now();
        contest.setCreationTime(time);
        contest.setPhase(Phase.FIRST);
        contest.setPhaseOneExpiration(time.plusDays(contestDto.getPhaseOneDays()));
        contest.setPhaseTwoExpiration(time.plusDays(contestDto.getPhaseOneDays())
                .plusHours(contestDto.getPhaseTwoHours()));
        return contest;
    }

    public Contest dtoToObject(ContestDto contestDto,int contestId) {
        try {
            Contest contest = dtoToObject(contestDto);
            contest.setId(contestId);
            return contest;
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

}
