package com.example.photocontestproject.models.dtos;

import com.example.photocontestproject.models.Photo;

import javax.validation.constraints.NotEmpty;

public class EntryDto {

    private int id;

    @NotEmpty(message = "Title field cannot be empty")
    private String title;

    @NotEmpty(message = "Story field cannot be empty")
    private String story;

    private int userId;

    private int contestId;

    private Photo photo;

    public EntryDto() {
    }

    public EntryDto(int contestId, String title, String story, Photo photo) {
        this.contestId = contestId;
        this.title = title;
        this.story = story;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }
}
