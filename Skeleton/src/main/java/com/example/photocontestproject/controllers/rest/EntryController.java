package com.example.photocontestproject.controllers.rest;

import com.example.photocontestproject.controllers.AuthenticationHelper;
import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Review;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.dtos.EntryDto;
import com.example.photocontestproject.models.dtos.ReviewDto;
import com.example.photocontestproject.services.contracts.EntryService;
import com.example.photocontestproject.services.contracts.ReviewService;
import com.example.photocontestproject.utils.mappers.EntryMapper;
import com.example.photocontestproject.utils.mappers.ReviewMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/contests/{id}/entries")
public class EntryController {

    private final EntryService entryService;
    private final EntryMapper entryMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ReviewService reviewService;
    private final ReviewMapper reviewMapper;

    @Autowired
    public EntryController(EntryService entryService, EntryMapper entryMapper, AuthenticationHelper authenticationHelper,
                           ReviewService reviewService, ReviewMapper reviewMapper) {
        this.entryService = entryService;
        this.entryMapper = entryMapper;
        this.authenticationHelper = authenticationHelper;
        this.reviewService = reviewService;
        this.reviewMapper = reviewMapper;
    }

    @GetMapping
    public List<Entry> getAllByContestId(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        return entryService.getAllByContestId(id);
    }


    @PostMapping("/new")
    public Entry createEntry(@RequestHeader HttpHeaders headers, @Valid @RequestBody EntryDto entryDto) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        Entry entry = entryMapper.dtoToObject(entryDto);
        try {
            entryService.create(entry, loggedUser);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return entryService.getByTitle(entryDto.getTitle());

    }

    @DeleteMapping("/{entryId}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int entryId) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        try {
            entryService.delete(entryId, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{entryId}/review")
    public Review createReview(@RequestHeader HttpHeaders headers, @PathVariable int entryId,
                               @Valid @RequestBody ReviewDto reviewDto) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        Review review = reviewMapper.fromDto(reviewDto);
        reviewService.create(review, loggedUser);
        return review;
    }

    @GetMapping("/search")
    public List<Entry> searchByTitle(@RequestHeader HttpHeaders headers,
                                     @RequestParam(required = false) Optional<String> search) {
        User loggedUser = authenticationHelper.tryGetUser(headers);
        return entryService.search(search, loggedUser);
    }
}
