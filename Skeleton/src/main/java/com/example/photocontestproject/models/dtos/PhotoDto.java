package com.example.photocontestproject.models.dtos;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

public class PhotoDto {

    private int id;

    @NotNull
    private String pathOrUrl;

    public PhotoDto() {
    }

    public PhotoDto(int id, String pathOrUrl) {
        this.id = id;
        this.pathOrUrl = pathOrUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPathOrUrl() {
        return pathOrUrl;
    }

    public void setPathOrUrl(String pathOrUrl) {
        this.pathOrUrl = pathOrUrl;
    }
}
