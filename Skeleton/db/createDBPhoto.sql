CREATE DATABASE IF NOT EXISTS `photo`;
USE `photo`;

create or replace table categories
(
    category_id int auto_increment
        primary key,
    name        varchar(64) not null,
    constraint categories_name_uindex
        unique (name)
);

create or replace table contests
(
    contest_id           int auto_increment
        primary key,
    title                varchar(500)         not null,
    category_id          int                  not null,
    open_access          tinyint(1) default 1 not null,
    creation_time        datetime             null,
    phase                int                  null,
    phase_one_expiration datetime             null,
    phase_two_expiration datetime             null,
    constraint contests_title_uindex
        unique (title),
    constraint contests_categories_id_fk
        foreign key (category_id) references categories (category_id)
);

create or replace table photos
(
    photo_id       int auto_increment
        primary key,
    is_cover_photo tinyint(1) default 0 null,
    is_url         tinyint(1) default 0 not null,
    path           varchar(8000)        not null,
    constraint photos_path_uindex
        unique (path)
);

create or replace table contests_photos
(
    cover_photo_id int auto_increment
        primary key,
    contest_id     int not null,
    photo_id       int not null,
    constraint cover_photos_contests_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint cover_photos_photos_id_fk
        foreign key (photo_id) references photos (photo_id)
);

create or replace table roles
(
    role_id int auto_increment
        primary key,
    role    varchar(50) not null,
    constraint roles_role_uindex
        unique (role)
);

create or replace table users
(
    user_id       int auto_increment
        primary key,
    first_name    varchar(64)   not null,
    last_name     varchar(64)   not null,
    username      varchar(64)   not null,
    password      varchar(64)   not null,
    email         varchar(100)  not null,
    points        int default 0 null,
    ranking       int default 0 null,
    creation_time datetime      not null,
    role_id       int           null,
    constraint users_email_uindex
        unique (email),
    constraint users_username_uindex
        unique (username),
    constraint users_roles_role_id_fk
        foreign key (role_id) references roles (role_id)
);

create or replace table entries
(
    entry_id   int auto_increment
        primary key,
    contest_id int                  not null,
    title      varchar(500)         not null,
    story      varchar(2000)        not null,
    is_invited tinyint(1) default 0 not null,
    user_id    int                  not null,
    photo_id   int                  not null,
    constraint entries_contests_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint entries_photos_id_fk
        foreign key (photo_id) references photos (photo_id),
    constraint entries_users_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table users_contests
(
    jury_id    int auto_increment
        primary key,
    contest_id int not null,
    user_id    int not null,
    constraint users_contests_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint users_contests_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table reviews
(
    review_id int auto_increment
        primary key,
    score     int  default 3         not null,
    comment   text default 'default' not null,
    is_wrong_category tinyint(1) default 0 not null,
    juror_id  int                    not null,
    entry_id  int                    not null,
    constraint reviews_entries_entry_id_fk
        foreign key (entry_id) references entries (entry_id),
    constraint reviews_users_user_id_fk
        foreign key (juror_id) references users (user_id)
);

create or replace table contests_users
(
    invite_id      int auto_increment
        primary key,
    contest_id     int not null,
    sender_user_id int not null,
    receiver_id       int not null,
    constraint invites_contests_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint invites_users_sender_id_fk
        foreign key (sender_user_id) references users (user_id),
    constraint invites_users_receiver_id_fk
        foreign key (receiver_id) references users (user_id)
);

