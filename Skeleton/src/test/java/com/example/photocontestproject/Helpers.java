package com.example.photocontestproject;

import com.example.photocontestproject.models.*;
import com.example.photocontestproject.models.enums.Phase;
import com.example.photocontestproject.models.enums.Ranking;

import java.time.LocalDateTime;
import java.util.HashSet;

public class Helpers {

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        mockUser.setRole(createMockRoleUser());
        mockUser.setPoints(0);
        mockUser.setRanking(Ranking.JUNKIE);
        mockUser.setCreationTime(LocalDateTime.now());
        return mockUser;
    }

    public static User createMockUserTest() {
        var mockUser = new User();
        mockUser.setId(3);
        mockUser.setFirstName("MockTest");
        mockUser.setLastName("MockTests");
        mockUser.setEmail("mockr@user.com");
        mockUser.setUsername("MockTestUserName");
        mockUser.setPassword("MockTestPassword");
        mockUser.setRole(createMockRoleUser());
        mockUser.setPoints(0);
        mockUser.setRanking(Ranking.JUNKIE);
        mockUser.setCreationTime(LocalDateTime.now());
        return mockUser;
    }

    public static User createMockOrganizer() {
        var mockUser = new User();
        mockUser.setId(2);
        mockUser.setFirstName("MockFirstNameOrg");
        mockUser.setLastName("MockLastNameOrg");
        mockUser.setEmail("mock@userOrg.com");
        mockUser.setUsername("MockUsernameOrg");
        mockUser.setPassword("MockPasswordOrg");
        mockUser.setRole(createMockRoleOrganizer());
        mockUser.setPoints(2000);
        mockUser.setRanking(Ranking.DICTATOR);
        mockUser.setCreationTime(LocalDateTime.now());
        return mockUser;
    }

    public static Contest createMockOpenContest() {
        var mockContest = new Contest();
        mockContest.setId(1);
        mockContest.setTitle("MockTitle");
        mockContest.setCategory(createMockCategory());
        mockContest.setOpenAccess(true);
        mockContest.setCreationTime(LocalDateTime.now());
        mockContest.setPhase(Phase.FIRST);
        mockContest.setJury(new HashSet<>());
        mockContest.setCoverPhoto(createMockPhotoCoverFile());
        mockContest.setEntries(new HashSet<>());
        mockContest.setPhaseOneExpiration(LocalDateTime.now().plusDays(1));
        mockContest.setPhaseTwoExpiration(mockContest.getPhaseOneExpiration().plusHours(12));
        return mockContest;
    }

    public static Contest createMockInviteContest() {
        var mockContest = new Contest();
        mockContest.setId(2);
        mockContest.setTitle("MockTitleInvite");
        mockContest.setCategory(createMockCategory());
        mockContest.setOpenAccess(false);
        mockContest.setCreationTime(LocalDateTime.now());
        mockContest.setPhase(Phase.FIRST);
        mockContest.setJury(new HashSet<>());
        mockContest.setCoverPhoto(createMockPhotoCoverFile());
        mockContest.setEntries(new HashSet<>());
        mockContest.setPhaseOneExpiration(LocalDateTime.now().plusDays(1));
        mockContest.setPhaseTwoExpiration(mockContest.getPhaseOneExpiration().plusHours(12));
        return mockContest;
    }

    public static Review createMockReview() {
        var mockReview = new Review();
        mockReview.setId(1);
        mockReview.setScore(10);
        mockReview.setComment("MockComment");
        mockReview.setJuror(createMockOrganizer());
        mockReview.setEntry(createMockEntry());
        mockReview.setIsWrongCategory(false);
        return mockReview;
    }

    public static Entry createMockEntry() {
        var mockEntry = new Entry();
        mockEntry.setId(1);
        mockEntry.setContest(createMockOpenContest());
        mockEntry.setTitle("MockTitle");
        mockEntry.setStory("MockStory");
        mockEntry.setInvited(false);
        mockEntry.setPhoto(createMockPhotoEntry());
        mockEntry.setParticipant(createMockUser());
        mockEntry.setReview(new HashSet<>());
        return mockEntry;
    }

    public static Invite createMockInvite() {
        var mockInvite = new Invite();
        mockInvite.setId(1);
        mockInvite.setContest(createMockInviteContest());
        mockInvite.setSender(createMockOrganizer());
        mockInvite.setReceiver(createMockUser());
        return mockInvite;
    }

    public static Photo createMockPhotoEntry() {
        var mockPhoto = new Photo();
        mockPhoto.setId(1);
        mockPhoto.setCoverPhoto(false);
        mockPhoto.setUrl(false);
        mockPhoto.setPath("/images/upload/The door.jpg");
        return mockPhoto;
    }

    public static Photo createMockPhotoCoverFile() {
        var mockPhoto = new Photo();
        mockPhoto.setId(2);
        mockPhoto.setCoverPhoto(true);
        mockPhoto.setUrl(false);
        mockPhoto.setPath("/images/upload/Lion.jpg");
        return mockPhoto;
    }

    public static Photo createMockPhotoCoverURL() {
        var mockPhoto = new Photo();
        mockPhoto.setId(3);
        mockPhoto.setCoverPhoto(true);
        mockPhoto.setUrl(true);
        mockPhoto.setPath("https://cdn.pixabay.com/photo/2022/04/18/16/16/ship-7140939_960_720.jpg");
        return mockPhoto;
    }

    public static Category createMockCategory() {
        var mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setName("MockCategory");
        mockCategory.setContests(new HashSet<>());
        return mockCategory;
    }

    public static Role createMockRoleUser() {
        var mockUserRole = new Role();
        mockUserRole.setId(2);
        mockUserRole.setRole("User");
        mockUserRole.setUsers(new HashSet<>());
        return mockUserRole;
    }

    public static Role createMockRoleOrganizer() {
        var mockOrganizerRole = new Role();
        mockOrganizerRole.setId(1);
        mockOrganizerRole.setRole("Organizer");
        mockOrganizerRole.setUsers(new HashSet<>());
        return mockOrganizerRole;
    }

}
