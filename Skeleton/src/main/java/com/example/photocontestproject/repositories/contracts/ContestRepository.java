package com.example.photocontestproject.repositories.contracts;

import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Phase;

import java.util.List;
import java.util.Optional;

public interface ContestRepository extends BaseCRUDRepository<Contest> {

    List<Contest> getAllNotFinished();

    List<Contest> getOpenByUser(User user);

    int getCountAllContests();

    List<Contest> getFinishedByUser(User user);

    List<Contest> getAllInPhaseWithOrWithoutInvites(int userId, Phase phase, int maxResults);

    List<Contest> getMostRecentlyFinished();

    List<Contest> filter(Optional<String> title, Optional<String> openAccess,
                         Optional<Phase> phase, Optional<Integer> categoryId);

}
